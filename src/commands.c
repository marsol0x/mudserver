/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

// NOTE: Thinking about commands:
//
// To model a bit off of PennMUSH we should have a central location that splits
// the incoming arguments based on flags or parameters we set in the command
// structure for a given command. That way we can write command functions that
// only do the work required of them, rather than trying to parse a string to
// tease out its inputs. It also allows us to detect invalid inputs (in the
// case of number of parameters, at least) before ever handling the command
// itself.
//
// One thing that PennMUSH did was write a hashtable to determine whether or
// not a command even existed, which likely sped up detection of non-existant
// commands. It's worth considering, especially because I had never written a
// hashtable myself. -- Except, thinking about it, why not just use the
// hashtable to store the commands? -- Aliases?

#include "commands.h"

COMMAND(cmd_say)
{
    if (rightargs)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};
        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s says, \"%s\"", entity->name, rightargs);

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    }
}

COMMAND(cmd_emit)
{
    if (*rightargs)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};

        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s", rightargs);

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    }
}

COMMAND(cmd_pose)
{
    if (*rightargs)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};

        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s %s", entity->name, rightargs);

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    }
}

COMMAND(cmd_look)
{
    Entity *t = 0;
    if (rightargs)
    {
        u8 *target = get_first_word(rightargs);
        t = entity_get_from_string(gameState, entity, target);
        free(target);
    }

    if (t)
    {
        switch (t->type)
        {
            case EntityType_Room:
            {
                do_look_room(gameState, entity, t);
            } break;

            case EntityType_Player:
            {
                do_look_user(entity, t);
            } break;

            case EntityType_Thing:
            {
                do_look_thing(entity, t);
            } break;

            default:
            {
                assert(!"Invalid code path");
            } break;
        }
    } else if (!rightargs) {
        Entity *loc = entity_search(gameState, entity->location);
        do_look_room(gameState, entity, loc);
    } else {
        notify_entity(entity, "What are you looking at?");
    }
}

COMMAND(cmd_who)
{
    Connection *conn;
    Entity *player;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len = 0;

    len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Connected Players:");
    for (conn = gameState->connections->next; conn != gameState->connections; conn = conn->next)
    {
        player = conn->entity;
        assert(player);

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n\t%s (#%d)", player->name, player->id);
    }

    notify_entity(entity, output_buffer);
}

COMMAND(cmd_where)
{
    Connection *conn;
    Entity *player, *loc;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len = 0;

    len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Connected Players:");
    for (conn = gameState->connections->next; conn != gameState->connections; conn = conn->next)
    {
        player = conn->entity;
        assert(player);
        loc = entity_search(gameState, player->location);

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n\t%s(#%d)\t%s(#%d)", player->name, player->id, loc->name, loc->id);
    }

    notify_entity(entity, output_buffer);
}

COMMAND(cmd_ooc)
{
    if (rightargs)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};
        i8 *ooc_str = "(OOC)";
        if (rightargs[0] == TOKEN_POSE)
        {
            snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s %s %s", ooc_str, entity->name, rightargs + 1);
        } else {
            snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s %s says, \"%s\"", ooc_str, entity->name, rightargs);
        }

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    }
}

COMMAND(cmd_page)
{
    u8 targetBuffer[MAX_BUFFER_SIZE] = {0};
    u8 selfBuffer[MAX_BUFFER_SIZE] = {0};
    i32 targetLen, selfLen;
    u8 *message = rightargs;
    i32 messageLen = (i32)strlen(message);
    Entity *target;

    if (leftargs)
    {
        target = entity_find_by_name(gameState->db, leftargs);
    } else {
        target = entity_search(gameState, entity->lastPaged);
    }

    if (target && target->type == EntityType_Player)
    {
        if (target->connection)
        {
            selfLen = snprintf(selfBuffer, MAX_BUFFER_SIZE - 1, "You paged %s with: ", target->name);
            targetLen = snprintf(targetBuffer, MAX_BUFFER_SIZE - 1, "%s paged you with: ", entity->name);

            if ((selfLen + messageLen >= MAX_BUFFER_SIZE) || (targetLen + messageLen >= MAX_BUFFER_SIZE))
            {
                notify_entity(entity, "Page message too large.");
            } else {
                snprintf(selfBuffer + selfLen, MAX_BUFFER_SIZE - selfLen - 1, "\"%s\"", message);
                snprintf(targetBuffer + targetLen, MAX_BUFFER_SIZE - targetLen - 1, "\"%s\"", message);
                notify_entity(target, targetBuffer); 
                notify_entity(entity, selfBuffer);

                entity->lastPaged = target->id;
            }
        } else {
            snprintf(selfBuffer, MAX_BUFFER_SIZE - 1, "%s is not online.", target->name);
            notify_entity(entity, selfBuffer);
        }
    } else {
        notify_entity(entity, "No such user.");
    }
}

COMMAND(cmd_quit)
{
    entity->connection->isConnected = false;
}

COMMAND(cmd_inventory)
{
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    u32 index;
    i32 len = 0;

    if (dynamic_array_size(entity->contents))
    {
        len += snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "Carrying:");
        for (index = 0; index < dynamic_array_size(entity->contents); ++index)
        {
            Entity *e = entity_search(gameState, entity->contents[index]);
            if (e)
            {
                len += snprintf(output_buffer + len, MAX_NAME_SIZE - len - 1, "\n%s (#%d)", e->name, e->id);
            }
        }
    } else {
        len += snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "You're not carrying anything.");
    }

    notify_entity(entity, output_buffer);
}

COMMAND(cmd_get)
{
    char outputBuffer[MAX_BUFFER_SIZE];
    i32 len = 0;
    Entity *object;

    if (rightargs)
    {
        object = entity_get_from_string(gameState, entity, rightargs);

        if (object)
        {
            entity_add_content(gameState, entity, object);
            len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "Picked up %s.", object->name);
        } else {
            len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "No such object here called %s.", rightargs);
        }

        notify_entity(entity, outputBuffer);
    }
}

COMMAND(cmd_drop)
{
    char outputBuffer[MAX_BUFFER_SIZE];
    i32 len = 0;
    Entity *object;
    Entity *entityLocation;

    if (rightargs)
    {
        entityLocation = entity_search(gameState, entity->location);
        object = entity_get_from_string(gameState, entity, rightargs);

        if (object)
        {
            entity_add_content(gameState, entityLocation, object);
            len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "Dropped %s.", object->name);
        } else {
            len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "No such object in your inventory called %s.", rightargs);
        }

        notify_entity(entity, outputBuffer);
    }
}

COMMAND(cmd_shutdown)
{
    Connection *c;

    gameState->running = false;
    for (c = gameState->connections->next; c != gameState->connections; c = c->next)
    {
        notify_entity(c->entity, "Server is now shutting down.");
    }
}

#if 0
COMMAND(cmd_examine)
{
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};

    if (!target)
    {
        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "Examine who?");
    } else {
        i32 i;
        i32 len = 0;
        Entity *loc;

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "%s (#%d)", target->name, target->id);
        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nConnected: %s", target->isConnected == true ? "Yes" : "No");
        if (target->location != -1)
        {
            loc = entity_search(gameState, target->location);
            assert(loc);
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nLocation: %s (#%d)", loc->name, loc->id);
        }
        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nChannels:");
        for (i = 0; i < MAX_CHANNEL_MEMBERSHIP; ++i)
        {
            if (*(target->channels + i))
            {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, " %s", *(target->channels + i));
            }
        }
        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nDescription: %s", target->description);
    }

    notify_entity(entity, output_buffer);
}
#endif

COMMAND(cmd_describe)
{
    if (leftargs)
    {
        Entity *target = entity_get_from_string(gameState, entity, leftargs);
        if (!target)
        {
            notify_entity(entity, "Invalid target.");
        } else {
            if (rightargs)
            {
                memcpy(target->description, rightargs, MAX_DESCRIPTION_SIZE - 1);
                notify_entity(entity, "Description updated.");
            } else {
                notify_entity(entity, "A description is needed.");
            }
        }
    } else {
        notify_entity(entity, "Who are you describing?");
    }
}

// TODO(marshel): Move this sorting stuff somewhere more central for more
// widespread use.
i32 sort_channels_partition(ChatChannel **arr, i32 low, i32 high)
{
    ChatChannel *temp;
    ChatChannel *pivot = arr[high];
    i32 i, j;
    i = low - 1;
    for (j = low; j < high; ++j)
    {
        i64 len = strlen(arr[j]->name);
        if (string_compare_insensitive(arr[j]->name, pivot->name, len) <= 0)
        {
            ++i;
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }
    temp = arr[i + 1];
    arr[i + 1] = arr[high];
    arr[high] = temp;

    return i + 1;
}

void sort_channels(ChatChannel **arr, i32 low, i32 high)
{
    if (low < high)
    {
        i32 part = sort_channels_partition(arr, low, high);
        sort_channels(arr, low, part - 1);
        sort_channels(arr, part + 1, high);
    }
}

COMMAND(cmd_channel)
{
    // NOTE(marshel):
    // + @channel/(add|remove) <target>=<channel>
    // + @channel/list
    // TODO(marshel):
    // + @channel/new <channel>

    i32 len = 0;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    Entity *target = 0;
    ChatChannel *channel;

    if (strcmp(sw, "add") == 0)
    {
        if (!leftargs)
        {
            notify_entity(entity, "A player target is required.");
            return;
        } else {
            target = entity_get_from_string(gameState, entity, leftargs);
            if (!target || target->type != EntityType_Player)
            {
                notify_entity(entity, "Invalid player name.");
                return;
            }
        }

        channel = chat_get_channel(gameState, rightargs);
        if (!channel)
        {
            snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "Unknown channel named %s", rightargs);
            notify_entity(entity, output_buffer);
            return;
        }
        channel_add_member(gameState, channel, target);
        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "Added %s to channel %s", target->name, channel->name);
    } else if (strcmp(sw, "remove") == 0) {
        if (!leftargs)
        {
            notify_entity(entity, "A player target is required.");
            return;
        } else {
            target = entity_get_from_string(gameState, entity, leftargs);
            if (!target || target->type != EntityType_Player)
            {
                notify_entity(entity, "Invalid player name.");
                return;
            }
        }

        channel = chat_get_channel(gameState, rightargs);
        if (!channel)
        {
            snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "Unknown channel named %s", rightargs);
            notify_entity(entity, output_buffer);
            return;
        }
        channel_remove_member(gameState, channel, target);
        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "Removed %s from channel %s", target->name, channel->name);
    } else if (strcmp(sw, "new") == 0) {
        // TODO(marshel): Create new channels
        len += snprintf(output_buffer, MAX_BUFFER_SIZE, "Channel creation not yet implemented.");
    } else if (strcmp(sw, "list") == 0) {
        i32 entityChannelCount = 0;
        ChatChannel *entityChannels[100] = {0};

        Queue stack;
        PrefixTree *channelTree = gameState->channels;

        if (rightargs)
        {
            target = entity_get_from_string(gameState, entity, rightargs);
            if (!target || target->type != EntityType_Player)
            {
                notify_entity(entity, "Invalid target.");
                return;
            }
        } else {
            target = entity;
        }

        if (channelTree->next)
        {
            i32 i;

            DLIST_INIT(&stack);
            queue_push(&stack, &channelTree->next, sizeof(&channelTree->next));

            while (stack.next != &stack)
            {
                PrefixTree *node;
                queue_pop(&stack, &node);

                if (node->data)
                {
                    ChatChannel *ch = (ChatChannel *)node->data;
                    if (channel_is_member(ch, entity->id))
                    {
                        entityChannels[entityChannelCount] = ch;
                        ++entityChannelCount;
                    }
                }

                if (node->child)
                {
                    queue_push(&stack, &node->child, sizeof(&node->child));
                }

                if (node->next)
                {
                    queue_push(&stack, &node->next, sizeof(&node->next));
                }
            }

            if (entityChannelCount > 0)
            {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE, "Channels:");
                sort_channels(entityChannels, 0, entityChannelCount - 1);
                for (i = 0; i < entityChannelCount; ++i)
                {
                    len += snprintf(output_buffer + len, MAX_BUFFER_SIZE + len + 1, " %s", entityChannels[i]->name);
                    if (i != entityChannelCount - 1)
                    {
                        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE + len + 1, ",");
                    }
                }
            } else {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE + len + 1, "You are not a memeber of any channels.");
            }
        }
    } else {
        notify_entity(entity, "Invalid switch.");
        return;
    }

    notify_entity(entity, output_buffer);
}

Token * nextToken(Tokenizer *t)
{
    Token *result;
    i8 *s = t->input + t->index;
    i32 startIndex;
    i32 endIndex;
    i32 len;

    if (t->index >= t->len)
    {
        return 0;
    }
    result = malloc(sizeof(*result));
    result->type = TokenType_Value;
    result->value = 0;
    startIndex = t->index;

    // NOTE(marshel): Skip leading whitespace
    while (*s && is_whitespace(*s)) { s++; startIndex++; }
    endIndex = result->index = startIndex;

    if (*s == '/')
    {
        result->type = TokenType_SwitchBreak;
        result->value = "/";
        endIndex++;
    } else if (*s == '=') {
        result->type = TokenType_Equals;
        result->value = "=";
        endIndex++;
    } else {
        while (*s && !is_whitespace(*s))
        {
            if (*s == '/' || *s == '=')
            {
                break;
            }
            s++;
            endIndex++;
        }
    }

    len = endIndex - startIndex;
    if (len > 0)
    {
        if (result->type == TokenType_Value)
        {
            result->value = malloc(len + 1);
            memset(result->value, 0, len + 1);
            memcpy(result->value, t->input + startIndex, len);
        }
    }
    t->index = endIndex;

    return result;
}

CommandFields * parseCommand(i8 *input)
{
    b32 hasCommand = false;
    b32 hasTarget = false;
    i32 argsIndex = 0;
    Tokenizer tokenizer;
    Token *token = 0; 
    Token *next;
    CommandFields *command = malloc(sizeof(*command));
    memset(command, 0, sizeof(*command));

    tokenizer.index = 0;
    tokenizer.len = (i32)strlen(input);
    tokenizer.input = malloc(tokenizer.len + 1);
    memset(tokenizer.input, 0, tokenizer.len + 1);
    memcpy(tokenizer.input, input, tokenizer.len);

    tokenizer.firstToken = malloc(sizeof(*tokenizer.firstToken));
    memset(tokenizer.firstToken, 0, sizeof(*tokenizer.firstToken));
    DLIST_INIT(tokenizer.firstToken);

    while ((token = nextToken(&tokenizer)))
    {
        DLIST_ADD(tokenizer.firstToken, token);
    }

    token = tokenizer.firstToken->prev;
    while (token != tokenizer.firstToken)
    {
        if (!hasCommand)
        {
            strcpy(command->cmd, token->value);
            hasCommand = true;
            if (token->prev->type == TokenType_SwitchBreak)
            {
                token = token->prev->prev;
                strcpy(command->sw, token->value);
            }
        } else {
            if (!hasTarget && token->prev->type == TokenType_Equals)
            {
                strcpy(command->leftargs, token->value);
                token = token->prev;
                hasTarget = true;
            } else {
                if (token->value)
                {
                    i32 len = argsIndex + (i32)strlen(token->value);
                    if (len < 1024)
                    {
                        if (argsIndex > 0)
                        {
                            strcat(command->rightargs, " ");
                        }
                        strcat(command->rightargs, token->value);
                        argsIndex += strlen(token->value);
                    } else {
                        break;
                    }
                }
            }
        }

        token = token->prev;
    }

    token = tokenizer.firstToken->next;
    while (token)
    {
        next = token->next;
        DLIST_REMOVE(token);
        if (token->type == TokenType_Value)
        {
            free(token->value);
        }
        if (token != tokenizer.firstToken)
        {
            free(token);
        } else {
            break;
        }
        token = next;
    }
    free(tokenizer.firstToken);
    free(tokenizer.input);

    return command;
}

void do_command(GameState *gameState, PrefixTree *commands, Entity *entity, u8 *buffer)
{
    u32 i;
    PrefixTree *node;
    Command *cmd;
    CommandFields *cf;
    Entity *loc = entity_search(gameState, entity->location);

    // NOTE(marshel): Parse command
    cf = parseCommand(buffer);

    // NOTE(marshel): First check command prefix tree
    node = ptree_find_nearest(commands, cf->cmd);
    if (node && node->data)
    {
        cmd = (Command *)node->data;
        cmd->cmd(gameState, entity, buffer, cf->sw, strlen(cf->leftargs) ? cf->leftargs : 0, strlen(cf->rightargs) ? cf->rightargs : 0);
        goto cf_cleanup;
    }

    // NOTE(marshel): Check channels    
    if (cf->cmd[0] == TOKEN_CHANNEL)
    {
        channel_message(gameState, entity, cf->cmd + 1, cf->rightargs);
        goto cf_cleanup;
    }

    // NOTE(marshel): Then check room exits
    // TODO(marshel): Moving around should be it's own function since it should
    // also facilitate teleporting
    assert(loc);
    for (i = 0; i < dynamic_array_size(loc->doors); ++i)
    {
        if (strncmp(loc->doors[i].name, cf->cmd, MAX_NAME_SIZE) == 0)
        {
            u32 index;
            Entity *loc  = entity_search(gameState, entity->location);    assert(loc);
            Entity *dest = entity_search(gameState, loc->doors[i].dest); assert(dest);

            for (index = 0; index < dynamic_array_size(loc->contents); ++index)
            {
                if (loc->contents[index] == entity->id)
                {
                    dynamic_array_erase(loc->contents, index);
                    break;
                }
            }

            entity->location = dest->id;
            dynamic_array_push(dest->contents, entity->id);
            do_look_room(gameState, entity, dest);
            goto cf_cleanup;
        }
    }

    notify_entity(entity, "Huh? Can't help you here.");

cf_cleanup:
    free(cf);
    cf = 0;
}
