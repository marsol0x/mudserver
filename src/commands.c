/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

// NOTE: Thinking about commands:
//
// To model a bit off of PennMUSH we should have a central location that splits
// the incoming arguments based on flags or parameters we set in the command
// structure for a given command. That way we can write command functions that
// only do the work required of them, rather than trying to parse a string to
// tease out its inputs. It also allows us to detect invalid inputs (in the
// case of number of parameters, at least) before ever handling the command
// itself.
//
// One thing that PennMUSH did was write a hashtable to determine whether or
// not a command even existed, which likely sped up detection of non-existant
// commands. It's worth considering, especially because I had never written a
// hashtable myself. -- Except, thinking about it, why not just use the
// hashtable to store the commands? -- Aliases?

#include "commands.h"
#include "parse_command.c"

COMMAND(cmd_say)
{
    if (args)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};
        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s says, \"%s\"", entity->name.str, args);

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    } else {
        notify_entity(entity, "Say what?");
    }
}

COMMAND(cmd_emit)
{
    if (args)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};

        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s", args);

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    } else {
        notify_entity(entity, "Emit what?");
    }
}

COMMAND(cmd_pose)
{
    if (args)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};

        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s %s", entity->name.str, args);

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    } else {
        notify_entity(entity, "Pose what?");
    }
}

COMMAND(cmd_look)
{
    Entity *t = 0;
    if (args)
    {
        u8 *target = get_first_word(&gameState->temporary_memory, args);
        t = entity_get_from_string(gameState, entity, target);
    }

    if (t)
    {
        switch (t->type)
        {
            case EntityType_Room:
            {
                do_look_room(gameState, entity, t);
            } break;

            case EntityType_Player:
            {
                do_look_user(entity, t);
            } break;

            case EntityType_Thing:
            {
                do_look_thing(entity, t);
            } break;

            default:
            {
                assert(!"Invalid code path");
            } break;
        }
    } else if (!args) {
        Entity *loc = entity_search(gameState, entity->location);
        do_look_room(gameState, entity, loc);
    } else {
        notify_entity(entity, "What are you looking at?");
    }
}

COMMAND(cmd_examine)
{
    if (args)
    {
        Entity *t = 0;
        u8 *target = get_first_word(&gameState->temporary_memory, args);
        t = entity_get_from_string(gameState, entity, target);

        if (t)
        {
            u8 buffer[MAX_BUFFER_SIZE] = {0};
            u8 *type_string = 0;
            i32 len = 0;
            i32 index;

            type_string = entity_type_to_cstring(&gameState->temporary_memory, t->type);

            len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "(#%d) %s%s%s", t->id, COLOR_FG_WHITE, t->name.str, COLOR_RESET);
            len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\nType: %s", type_string);
            if (*t->alias) len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\nAlias: %s", t->alias);
            len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\nDescription: %s", t->description);

            if (t->location != -1)
            {
                Entity *location = entity_search(gameState, t->location);
                if (location)
                {
                    len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\nLocation: %s(#%d)", location->name.str, location->id);
                }
            }

            if (t->lastPaged != -1)
            {
                Entity *lastPaged = entity_search(gameState, t->lastPaged);
                if (lastPaged)
                {
                    len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\nLastPaged: %s(#%d)", lastPaged->name.str, lastPaged->id);
                }
            }

            if (dynamic_array_size(t->contents))
            {
                len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\nContents:");
                for (index = 0;
                     index < dynamic_array_size(t->contents);
                     ++index)
                {
                    Entity *content = entity_search(gameState, t->contents[index]);
                    if (content)
                    {
                        len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\n%s(#%d)", content->name.str, content->id);
                    }
                }
            }

            if (dynamic_array_size(t->doors))
            {
                len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\nDoors:");
                for (index = 0;
                    index < dynamic_array_size(t->doors);
                    ++index)
                {
                    Door door = t->doors[index];
                    Entity *dest = entity_search(gameState, door.dest);
                    if (dest)
                    {
                        len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\n%s->(#%d)%s", door.name.str, dest->id, dest->name.str);
                    } else {
                        len += snprintf(buffer + len, MAX_BUFFER_SIZE - len - 1, "\n%s->(#-1)Broken Link", door.name.str);
                    }
                }
            }

            notify_entity(entity, buffer);
        } else {
            notify_entity(entity, "No such thing nearby.");
        }
    } else {
        notify_entity(entity, "Explain what?");
    }
}

COMMAND(cmd_who)
{
    Connection *conn;
    Entity *player;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len = 0;

    len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Connected Players:");
    for (conn = gameState->connections.next; conn != &gameState->connections; conn = conn->next)
    {
        player = conn->entity;
        assert(player);

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n\t%s (#%d)", player->name.str, player->id);
    }

    notify_entity(entity, output_buffer);
}

COMMAND(cmd_where)
{
    Connection *conn;
    Entity *player, *loc;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len = 0;

    len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Connected Players:");
    for (conn = gameState->connections.next; conn != &gameState->connections; conn = conn->next)
    {
        player = conn->entity;
        assert(player);
        loc = entity_search(gameState, player->location);

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n\t%s(#%d)\t%s(#%d)", player->name.str, player->id, loc->name.str, loc->id);
    }

    notify_entity(entity, output_buffer);
}

COMMAND(cmd_ooc)
{
    if (args)
    {
        Entity *room;
        u8 output_buffer[MAX_BUFFER_SIZE] = {0};
        i8 *ooc_str = "(OOC)";
        if (args[0] == TOKEN_POSE)
        {
            snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s %s %s", ooc_str, entity->name.str, args + 1);
        } else {
            snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s %s says, \"%s\"", ooc_str, entity->name.str, args);
        }

        room = entity_search(gameState, entity->location);
        notify_room(gameState, room, output_buffer);
    }
}

COMMAND(cmd_page)
{
    u8 *message = args;
    Entity *targetEntity;

    if (target)
    {
        targetEntity = entity_find_by_name(gameState->db, target);
    } else {
        targetEntity = entity_search(gameState, entity->lastPaged);
    }

    if (targetEntity && targetEntity->type == EntityType_Player)
    {
        if (targetEntity->connection)
        {
            notify_entity(entity, "You paged %s with: \"%s\"", targetEntity->name.str, message);
            notify_entity(targetEntity, "%s paged you with: \"%s\"", entity->name.str, message);

            entity->lastPaged = targetEntity->id;
        } else {
            notify_entity(entity, "%s is not online.", targetEntity->name.str);
        }
    } else {
        notify_entity(entity, "No such user.");
    }
}

COMMAND(cmd_quit)
{
    entity->connection->isConnected = false;
}

COMMAND(cmd_inventory)
{
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len = 0;

    if (dynamic_array_size(entity->contents))
    {
        u32 index;

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Carrying:");
        for (index = 0;
             index < dynamic_array_size(entity->contents);
             ++index)
        {
            Entity *e = entity_search(gameState, entity->contents[index]);
            if (e)
            {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n%s(#%d)", e->name.str, e->id);
            }
        }
    } else {
        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "You're not carrying anything.");
    }

    notify_entity(entity, output_buffer);
}

COMMAND(cmd_get)
{
    Entity *object;

    if (args)
    {
        object = entity_get_from_string(gameState, entity, args);

        if (object)
        {
            if (object->location == entity->id)
            {
                notify_entity(entity, "You're already carrying that!");
            } else {
                entity_add_content(gameState, entity, object);
                notify_entity(entity, "Picked up %s.", object->name.str);
            }
        } else {
            notify_entity(entity, "No such object here called %s.", args);
        }
    }
}

COMMAND(cmd_drop)
{
    Entity *object;
    Entity *entityLocation;

    if (args)
    {
        entityLocation = entity_search(gameState, entity->location);
        object = entity_get_from_string(gameState, entity, args);

        if (object)
        {
            entity_add_content(gameState, entityLocation, object);
            notify_entity(entity, "Dropped %s.", object->name.str);
        } else {
            notify_entity(entity, "no such object in your inventory called %s.", args);
        }
    }
}

COMMAND(cmd_shutdown)
{
    gameState->running = false;
    notify_all_connected(gameState, "Server is now shutting down.");
}

COMMAND(cmd_password)
{
    // NOTE(marshel):
    // + @password <password>
    if (args)
    {
        memset(entity->password, 0, MAX_PASSWORD_SIZE);
        memcpy(entity->password, args, strlen(args));
        notify_entity(entity, "Password changed.");
    } else {
        notify_entity(entity, "Change your password to what?");
    }
}

COMMAND(cmd_name)
{
    // NOTE(marshel):
    // + @name [<taget>=]<name>

    Entity *targetEntity = 0;
    String oldName;

    if (target)
    {
        targetEntity = entity_get_from_string(gameState, entity, target);
        if (!targetEntity)
        {
            notify_entity(entity, "What are you naming?");
            return;
        }
    } else {
        targetEntity = entity;
    }

    if (!args)
    {
        notify_entity(entity, "What new name do you want to give to %s?", targetEntity->name.str);
        return;
    }

    oldName = targetEntity->name;
    targetEntity->name = string_from_cstring(args);
    if (oldName.str)
    {
        notify_entity(entity, "Changed name of %s to %s.", oldName.str, targetEntity->name.str);
        free(oldName.str);
    } else {
        notify_entity(entity, "Changed name of #%d to %s.", targetEntity->id, targetEntity->name.str);
    }
}

COMMAND(cmd_alias)
{
    // NOTE(marshel):
    // + @alias [<taget>=]<name>

    size_t aliasLength = 0;
    Entity *targetEntity = 0;
    if (target)
    {
        targetEntity = entity_get_from_string(gameState, entity, target);
        if (!targetEntity)
        {
            notify_entity(entity, "What are you setting an alias for?");
            return;
        }
    } else {
        targetEntity = entity;
    }

    if (!args)
    {
        notify_entity(entity, "What new alias do you want to give to %s?", targetEntity->name.str);
        return;
    }

    aliasLength = strlen(args);
    if (aliasLength > MAX_NAME_SIZE - 1)
    {
        notify_entity(entity, "That alias is too long.");
    } else {
        strncpy(targetEntity->alias, args, aliasLength + 1);
        notify_entity(entity, "Alias set.");
    }
}

COMMAND(cmd_describe)
{
    // NOTE(marshel):
    // + @describe <targetEntity>=<description>
    // Describe self
    // + @describe <description>

    Entity *targetEntity = entity;
    if (target)
    {
        targetEntity = entity_get_from_string(gameState, entity, target);
        if (!targetEntity)
        {
            notify_entity(entity, "Invalid target.");
            return;
        }
    }

    if (args)
    {
        memcpy(targetEntity->description, args, MAX_DESCRIPTION_SIZE - 1);
        notify_entity(entity, "Description updated for %s.", targetEntity->name.str);
    } else {
        notify_entity(entity, "A description is needed.");
    }
}

COMMAND(cmd_channel)
{
    // NOTE(marshel):
    // + @channel/(add|remove) <targetEntity>=<channel>
    // + @channel/list [<targetEntity>]
    // + @channel/listall
    // + @channel/new <channel>
    // + @channel/log <channel>

    i32 len = 0;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    Entity *targetEntity = 0;
    ChatChannel *channel;

    if (strcmp(sw, "add") == 0)
    {
        if (!target)
        {
            notify_entity(entity, "A player targetEntity is required.");
            return;
        } else {
            targetEntity = entity_get_from_string(gameState, entity, target);
            if (!targetEntity || targetEntity->type != EntityType_Player)
            {
                notify_entity(entity, "Invalid player name.");
                return;
            }
        }

        channel = chat_get_channel(gameState, args);
        if (!channel)
        {
            notify_entity(entity, "Unknown channel named %s.", args);
        } else {
            channel_add_member(gameState, channel, targetEntity);
            notify_entity(entity, "Added %s to channel %s.", targetEntity->name.str, channel->name.str);
        }
    } else if (strcmp(sw, "remove") == 0) {
        if (!target)
        {
            notify_entity(entity, "A player targetEntity is required.");
            return;
        } else {
            targetEntity = entity_get_from_string(gameState, entity, target);
            if (!targetEntity || targetEntity->type != EntityType_Player)
            {
                notify_entity(entity, "Invalid player name.");
                return;
            }
        }

        channel = chat_get_channel(gameState, args);
        if (!channel)
        {
            notify_entity(entity, "Unknown channel named %s.", args);
        } else {
            channel_remove_member(gameState, channel, targetEntity);
            notify_entity(entity, "Removed %s from channel %s.", targetEntity->name.str, channel->name.str);
        }
    } else if (strcmp(sw, "new") == 0) {
        if (args)
        {
            ChatChannel *newChannel = chat_new_channel(gameState, args);
            if (newChannel)
            {
                notify_entity(entity, "Create channel %s.", args);
                channel_add_member(gameState, newChannel, entity);
            } else {
                notify_entity(entity, "Channel %s already exists.", args);
            }
        } else {
            notify_entity(entity, "What will be the name of this new channel?");
        }
    } else if (strcmp(sw, "list") == 0) {
        String *channelList = 0;

        if (args)
        {
            targetEntity = entity_get_from_string(gameState, entity, args);
            if (!targetEntity || targetEntity->type != EntityType_Player)
            {
                notify_entity(entity, "Invalid targetEntity.");
                return;
            }
        } else {
            targetEntity = entity;
        }

        channelList = channel_membership_list(gameState, targetEntity);
        if (channelList)
        {
            i32 channelIndex;

            string_sort(channelList, 0, dynamic_array_size(channelList) - 1);

            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - 1, "Channels:");
            for (channelIndex = 0;
                 channelIndex < dynamic_array_size(channelList);
                 ++channelIndex)
            {
                String *channel = channelList + channelIndex;
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, " %s", channel->str);
            }

            dynamic_array_free(channelList);
        } else {
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "%s is not a member of any channels.", targetEntity->name.str);
        }
        notify_entity(entity, output_buffer);
    } else if (strcmp(sw, "listall") == 0) {
        // TODO(marshel): List all available channels

    } else if (strcmp(sw, "log") == 0) {
        ChatChannel *channel;

        if (!target)
        {
            notify_entity(entity, "Channel name is required.");
            return;
        }

        channel = chat_get_channel(gameState, target);
        if (!channel)
        {
            notify_entity(entity, "Channel %s does not exist.", target);
            return;
        }

        if (!channel_is_member(channel, entity->id))
        {
            notify_entity(entity, "You must be a member of channel %s to view channel history.", channel->name.str);
            return;
        }

        notify_entity(entity, "Channel log goes here");
    } else {
        notify_entity(entity, "Invalid switch.");
        return;
    }
}

COMMAND(cmd_teleport)
{
    // NOTE(marshel):
    //    @teleport <destination>
    //    @teleport <targetEntity>=<destination>
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len = 0;
    Entity *targetEntity = 0;
    Entity *dest = 0;

    if (target)
    {
        targetEntity = entity_get_from_string(gameState, entity, target);
        if (!targetEntity)
        {
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Unknown targetEntity %s", target);
            return;
        }

        dest = entity_get_from_string(gameState, entity, args);
        if (!dest)
        {
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Unknown destination %s", target);
            return;
        }
    } else {
        targetEntity = entity;
        dest = entity_get_from_string(gameState, entity, args);

        if (!dest)
        {
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Unknown destination %s", target);
            return;
        }
    }

    entity_move(gameState, targetEntity, dest);
    do_look_room(gameState, targetEntity, dest);
}

void game_save(GameState *gameState);
COMMAND(cmd_dump)
{
    game_save(gameState);
}

COMMAND(cmd_create)
{
    // NOTE(marshel): Creates objects and places it in the player's inventory
    // TODO(marshel): Should this cost something? Be restricted in some way?
    // + @create <object name>

    Entity *thing = 0;
    if (!args)
    {
        notify_entity(entity, "@create takes a name parameter.");
        return;
    }

    thing = entity_create(gameState, entity->id, EntityType_Thing, args, "");
    entity_add_content(gameState, entity, thing);
}

COMMAND(cmd_destroy)
{
    // NOTE(marshel): Destroys an object
    // + @destroy <object>

    Entity *targetEntity = 0;
    if (!args)
    {
        notify_entity(entity, "What do you want to destroy?");
        return;
    }

    targetEntity = entity_get_from_string(gameState, entity, args);
    if (!targetEntity)
    {
        notify_entity(entity, "No such object called %s.", args);
        return;
    }

    if (targetEntity->type != EntityType_Thing)
    {
        notify_entity(entity, "Cannot destroy non-thing entities.");
        return;
    }

    {
        Entity *targetEntityLocation = entity_search(gameState, targetEntity->location);
        if (targetEntityLocation)
        {
            i32 index;
            for (index = 0;
                 index < dynamic_array_size(targetEntityLocation->contents);
                 ++index)
            {
                if (targetEntityLocation->contents[index] == targetEntity->id)
                {
                    dynamic_array_erase(targetEntityLocation->contents, index);
                    break;
                }
            }
        }
    }
    notify_entity(entity, "Destroyed %s.", targetEntity->name.str);
    entity_delete(gameState, targetEntity);
}

void do_command(GameState *gameState, PrefixTree *commands, Entity *entity, u8 *buffer)
{
    u32 i;
    PrefixTree *node;
    Command *cmd;
    CommandFields *cf;
    Entity *loc = 0;

    // NOTE(marshel): Parse command
    cf = parseCommand(&gameState->temporary_memory, buffer);

    // NOTE(marshel): Check channels    
    if (cf->cmd[0] == TOKEN_CHANNEL)
    {
        channel_message(gameState, entity, cf->cmd + 1, cf->args);
        return;
    }

    // NOTE(marshel): Check command prefix tree
    node = ptree_find_nearest(commands, cf->cmd);
    if (node && node->data)
    {
        cmd = (Command *)node->data;
        cmd->cmd(gameState, entity, buffer, cf->sw, strlen(cf->target) ? cf->target : 0, strlen(cf->args) ? cf->args : 0);
        return;
    }

    // NOTE(marshel): Then check room exits
    loc = entity_search(gameState, entity->location);
    assert(loc);
    for (i = 0; i < dynamic_array_size(loc->doors); ++i)
    {
        Door *door = loc->doors + i;
        Entity *dest = 0;
        if (string_compare_insensitive(door->name.str, cf->cmd, door->name.length) == 0)
        {
            dest = entity_search(gameState, door->dest);
        } else {
            for (u32 alias_index = 0;
                 alias_index < dynamic_array_size(door->aliases);
                 ++alias_index)
            {
                String *alias = door->aliases + alias_index;
                if (string_compare_insensitive(alias->str, cf->cmd, alias->length) == 0)
                {
                    dest = entity_search(gameState, door->dest);
                    break;
                }
            }
        }

        if (dest)
        {
            entity_move(gameState, entity, dest);

            do_look_room(gameState, entity, dest);
            return;
        }
    }

    notify_entity(entity, "Huh? Can't help you here.");
}
