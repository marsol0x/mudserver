#ifndef GAME_H
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */
#define VERSION 1
typedef struct connection_t
{
    struct connection_t *next, *prev;
    i32 sock;
    b32 isConnected;
    struct entity_t *entity;

    Queue input_queue;  // NOTE(marshel): To be processed input
    Queue output_queue; // NOTE(marshel): Output ready to go to sock
    i32 input_buffer_index;
    u8 input_buffer[MAX_BUFFER_SIZE];
} Connection;

typedef struct
{
    RBTree *db;
    MemoryArena temporary_memory;

    dbid starting_room;
    PrefixTree *channels;

    Queue output_queue;

    Connection connections;
    i32 nfds;
    fd_set masterfds;

    struct timer_t *timers;

    char *saveFilePath;
    b32 running;
} GameState;

#define GAME_H
#endif
