#ifndef GAME_H
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

#define VERSION 1
typedef struct entity_t Entity;
typedef struct connection_t
{
    struct connection_t *next, *prev;
    i32 sock;
    b32 isConnected;
    Entity *entity;

    Queue input_queue;  // NOTE(marshel): To be processed input
    Queue output_queue; // NOTE(marshel): Output ready to go to sock
    i32 input_buffer_index;
    u8 input_buffer[MAX_BUFFER_SIZE];
} Connection;

typedef struct
{
    RBTree *db;
    Entity *starting_room;
    PrefixTree *channels;

    Queue output_queue;

    Connection *connections;
    i32 nfds;
    fd_set masterfds;

    b32 running;
} GameState;

#define GAME_H
#endif
