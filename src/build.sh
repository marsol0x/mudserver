#!/bin/bash

errors="-Weverything"
disabled_errors="-Wno-missing-prototypes -Wno-padded -Wno-shadow -Wno-sign-compare -Wno-format-nonliteral -Wno-missing-field-initializers -Wno-switch-enum -Wno-missing-braces"
temp_disabled="-Wno-missing-variable-declarations -Wno-unreachable-code -Wno-unused-macros -Wno-unused-parameter -Wno-cast-align -Wno-pointer-sign -Wno-sign-conversion -Wno-incompatible-pointer-types-discards-qualifiers -Wno-string-conversion"
temp_disabled_osx="-Wno-unreachable-code-return -Wno-string-conversion -Wno-missing-noreturn"

if [ "`uname`" = "Darwin" ]
then
    temp_disabled="$temp_disabled $temp_disabled_osx"
else
    temp_disabled="$temp_disabled $temp_disabled_nix"
fi

mkdir -p ../bin
/usr/bin/clang -std=c99 -O0 -g $errors $disabled_errors $temp_disabled -o ../bin/mudserver main.c
#/usr/bin/clang -std=c99 -O0 -g $errors $disabled_errors $temp_disabled -o ../bin/parse_savefile parse_savefile.c
