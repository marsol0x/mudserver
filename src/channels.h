#ifndef CHANNELS_H
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#define MAX_CHANNEL_TABLE_SIZE 20

typedef struct _chat_channel_t
{
    String name;
    // NOTE(marshel): Dynamic array
    dbid *members;
} ChatChannel;

#define CHANNELS_H
#endif

