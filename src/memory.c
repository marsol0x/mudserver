/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */
void * xmalloc(size_t size)
{
    void *result = malloc(size);
    if (!result)
    {
        perror("xmalloc failed");
        exit(1);
    }

    return result;
}

void * xcalloc(size_t num, size_t size)
{
    void *result = calloc(num, size);
    if (!result)
    {
        perror("xcalloc failed");
        exit(1);
    }

    return result;
}

void * xrealloc(void *ptr, size_t new_size)
{
    void *result = realloc(ptr, new_size);
    if (!result)
    {
        perror("xrealloc failed");
        exit(1);
    }

    return result;
}

#define KILOBYTES(n) ((n) * 1024)
#define MEGABYTES(n) (KILOBYTES(n) * 1024)

#define BYTE_ALIGNMENT 4

typedef struct
{
    u32 size;
    u32 used;
    u32 high_water_mark;
    u8 *memory;
} MemoryArena;

b32 init_memory_arena(MemoryArena *arena, u32 size)
{
    b32 result = false;

    assert(arena);
    arena->memory = xcalloc(1, size);
    arena->size = size;
    arena->used = 0;

    return result;
}

void memory_arena_reset(MemoryArena *arena)
{
    arena->used = 0;
    memset(arena->memory, 0, arena->size);
}

void * memory_arena_allocate(MemoryArena *arena, size_t size)
{
    void *result = 0;
    size_t result_pointer = (size_t)arena->memory + arena->used;

    u32 alignment_offset = 0;
    u32 alignment_mask = BYTE_ALIGNMENT - 1;
    if (result_pointer & alignment_mask)
    {
        alignment_offset = BYTE_ALIGNMENT - (result_pointer & alignment_mask);
    }
    size += alignment_offset;

    assert(arena->used + size < arena->size);

    result = arena->memory + arena->used + alignment_offset;
    arena->used += size;

    if (arena->used > arena->high_water_mark)
    {
        arena->high_water_mark = arena->used;
    }

    printf("Memory Arena - Size: %u Used: %u High Water Mark: %u\n", arena->size, arena->used, arena->high_water_mark);

    return result;
}
