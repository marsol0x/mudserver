#ifndef SOCK_H
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#define MAXQUEUE 5

void write_to_sock(i32 sock, u8 *buffer, i64 bufferlen);

#define SOCK_H
#endif
