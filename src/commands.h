#ifndef COMMANDS_H
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

/*
 * TODO(marshel):
 *  Perhaps commands should be more active in the parsing process? PennMUSH
 *  does a left-arg/right-arg thing where it provides as strings the left and
 *  right of the `=`, rather than trying to grab a target itself; instead the
 *  command would determine whether or not to use a command. This does mean we
 *  cannot offload command validation check prior to calling the command
 *  function, but that might not be so bad. PennMUSH also provides the full,
 *  un-parsed command which might be useful for debugging later.
 *  Additionally, PennMUSH *does* allow for multiple swiches. For those I think
 *  we would parse and provide those as a single linked-list; assuming we even
 *  want to support that sort of thing.
 */

#define COMMAND(cmd) void cmd(GameState *gameState, Entity *entity, u8 *raw, u8 *sw, u8 *leftargs, u8 *rightargs)
typedef COMMAND(CommandFunction);

#define CMD_MAX_SIZE 31
#define MAX_SWITCH_COUNT 10
typedef struct
{
    u8 name[CMD_MAX_SIZE];
    CommandFunction *cmd;
} Command;

#define MAX_FIELD_SIZE 30
typedef struct
{
    i8 cmd[CMD_MAX_SIZE];
    i8 sw[MAX_FIELD_SIZE];
    // TODO(marshel): Consider allocating these on the fly 
    i8 leftargs[MAX_BUFFER_SIZE];
    i8 rightargs[MAX_BUFFER_SIZE];
} CommandFields;

enum
{
    TokenType_Value,
    TokenType_SwitchBreak,
    TokenType_Equals,
};

typedef struct token_t
{
    struct token_t *prev, *next;
    i32 type;
    i32 index;
    i8 *value;
} Token;

typedef struct
{
    i32 len;
    i32 index;
    Token *firstToken;
    i8 *input;
} Tokenizer;

b32 is_end_of_line(char c)
{
    return (c == '\n' || c == '\r');
}

b32 is_whitespace(char c)
{
    return (c == ' ' || c == '\t' || c == '\f' || c == '\v' || is_end_of_line(c));
}

void strip_whitespace(u8 *string)
{
    u8 *s;
    i32 i, j, ws;
    i = j = ws = 0;

    s = string;
    while (s[i])
    {
        ws = 0;
        s[i++] = s[j++];
        while (s[j + ws] && is_whitespace(s[j + ws]))
        {
            ws++;
        }

        if (ws > 1)
        {
            j += ws - 1;
        }
    }
}

u8 * get_first_word(u8 *str)
{
    u8 *result;
    u8 *s = str;
    u32 len;
    while (*s && !is_whitespace(*s)) { s++; }

    len = (u32)(s - str + 1);
    result = malloc(len);
    strncpy(result, str, len);
    result[len - 1] = 0;

    return result;
}

#define COMMANDS_H
#endif

