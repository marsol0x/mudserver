#ifndef COMMANDS_H
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

/*
 * TODO(marshel):
 *  Perhaps commands should be more active in the parsing process? PennMUSH
 *  does a left-arg/right-arg thing where it provides as strings the left and
 *  right of the `=`, rather than trying to grab a target itself; instead the
 *  command would determine whether or not to use a command. This does mean we
 *  cannot offload command validation check prior to calling the command
 *  function, but that might not be so bad. PennMUSH also provides the full,
 *  un-parsed command which might be useful for debugging later.
 *  Additionally, PennMUSH *does* allow for multiple swiches. For those I think
 *  we would parse and provide those as a single linked-list; assuming we even
 *  want to support that sort of thing.
 */

#define COMMAND(cmd) void cmd(GameState *gameState, Entity *entity, u8 *raw, u8 *sw, u8 *target, u8 *args)
typedef COMMAND(CommandFunction);

#define CMD_MAX_SIZE 31
#define MAX_SWITCH_COUNT 10
typedef struct
{
    u8 name[CMD_MAX_SIZE];
    CommandFunction *cmd;
} Command;

#define MAX_FIELD_SIZE 30
typedef struct
{
    i8 cmd[CMD_MAX_SIZE];
    i8 sw[MAX_FIELD_SIZE];
    // TODO(marshel): Consider allocating these on the fly 
    i8 target[MAX_BUFFER_SIZE];
    i8 args[MAX_BUFFER_SIZE];
} CommandFields;

#define COMMANDS_H
#endif

