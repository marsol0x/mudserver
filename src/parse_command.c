/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

// NOTE(marshel):
// + command/<sw> <leftargs>=<rightargs>
// + command <rightargs>
// + command/<sw> <rightargs>

// NOTE(marshel):
// + command/<switch> <target>=<args>
// + command <args>
// + command/<switch> <args>

enum
{
    TokenType_Value,
    TokenType_SwitchBreak,
    TokenType_Equals,
};

typedef struct token_t
{
    struct token_t *prev, *next;
    i32 type;
    i32 index;
    i8 *value;
} Token;

typedef struct
{
    i32 len;
    i32 index;
    Token *firstToken;
    i8 *input;
} Tokenizer;

u8 * get_first_word(MemoryArena *memory_arena, u8 *str)
{
    u8 *result;
    u8 *s = str;
    u32 len;
    while (*s && !is_whitespace(*s)) { s++; }

    len = (u32)(s - str + 1);
    result = memory_arena_allocate(memory_arena, len);
    strncpy(result, str, len);
    result[len - 1] = 0;

    return result;
}
Token * nextToken(MemoryArena *memory_arena, Tokenizer *t)
{
    Token *result;
    i8 *s = t->input + t->index;
    i32 startIndex;
    i32 endIndex;
    i32 len;

    if (t->index >= t->len)
    {
        return 0;
    }
    result = (Token *)memory_arena_allocate(memory_arena, sizeof(*result));
    result->type = TokenType_Value;
    result->value = 0;
    startIndex = t->index;

    // NOTE(marshel): Skip leading whitespace
    while (*s && is_whitespace(*s)) { s++; startIndex++; }
    endIndex = result->index = startIndex;

    if (*s == '/')
    {
        result->type = TokenType_SwitchBreak;
        result->value = "/";
        endIndex++;
    } else if (*s == '=') {
        result->type = TokenType_Equals;
        result->value = "=";
        endIndex++;
    } else {
        while (*s && !is_whitespace(*s))
        {
            if (*s == '/' || *s == '=')
            {
                break;
            }
            s++;
            endIndex++;
        }
    }

    len = endIndex - startIndex;
    if (len > 0)
    {
        if (result->type == TokenType_Value)
        {
            result->value = memory_arena_allocate(memory_arena, len + 1);
            memcpy(result->value, t->input + startIndex, len);
        }
    }
    t->index = endIndex;

    return result;
}

CommandFields * parseCommand(MemoryArena *memory_arena, i8 *input)
{
    b32 hasCommand = false;
    b32 hasTarget = false;
    i32 argsIndex = 0;
    Tokenizer tokenizer;
    Token *token = 0; 
    CommandFields *command = (CommandFields *)memory_arena_allocate(memory_arena, sizeof(CommandFields));

    tokenizer.index = 0;
    tokenizer.len = (i32)strlen(input);
    tokenizer.input = (i8 *)memory_arena_allocate(memory_arena, tokenizer.len + 1);
    memcpy(tokenizer.input, input, tokenizer.len);

    tokenizer.firstToken = (Token *)memory_arena_allocate(memory_arena, sizeof(Token));
    DLIST_INIT(tokenizer.firstToken);

    while ((token = nextToken(memory_arena, &tokenizer)))
    {
        DLIST_ADD(tokenizer.firstToken, token);
    }

    token = tokenizer.firstToken->prev;
    while (token != tokenizer.firstToken)
    {
        if (!hasCommand)
        {
            strcpy(command->cmd, token->value);
            hasCommand = true;
            if (token->prev->type == TokenType_SwitchBreak)
            {
                token = token->prev->prev;
                strcpy(command->sw, token->value);
            }
        } else {
            if (!hasTarget && token->prev->type == TokenType_Equals)
            {
                strcpy(command->target, token->value);
                token = token->prev;
                hasTarget = true;
            } else {
                if (token->value)
                {
                    i32 len = argsIndex + (i32)strlen(token->value);
                    if (len < 1024)
                    {
                        if (argsIndex > 0)
                        {
                            strcat(command->args, " ");
                        }
                        strcat(command->args, token->value);
                        argsIndex += strlen(token->value);
                    } else {
                        break;
                    }
                }
            }
        }

        token = token->prev;
    }

    return command;
}
