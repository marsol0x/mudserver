#ifndef QUEUE_H
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

#define DLIST_INIT(sentinal) \
    (sentinal)->next = (sentinal); \
    (sentinal)->prev = (sentinal)

#define DLIST_ADD(sentinal, node) \
    (node)->next = (sentinal)->next; \
    (node)->prev = (sentinal); \
    (sentinal)->next->prev = (node); \
    (sentinal)->next = (node)

#define DLIST_REMOVE(node) \
    (node)->prev->next = (node)->next; \
    (node)->next->prev = (node)->prev

typedef struct queue
{
    struct queue *next, *prev;
    u32 size;
    void *data;
} Queue;

void queue_push(Queue *q, void *data, u32 size)
{
    Queue *node = (Queue *)xcalloc(1, sizeof(*node));
    node->size = size;
    node->data = xcalloc(1, size);
    memcpy(node->data, data, size);

    DLIST_ADD(q, node);
}

u32 queue_pop(Queue *q, void *out)
{
    Queue *temp;
    u32 size;

    if (q->prev == q)
    {
        return 0;
    }

    temp = q->prev;
    size = temp->size;
    if (out == NULL)
    {
        return size;
    }
    DLIST_REMOVE(temp);
    memcpy(out, temp->data, size);
    free(temp->data);
    free(temp);

    return size;
}

#define QUEUE_H
#endif
