/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

void do_look_user(Entity *entity, Entity *target)
{
    i32 len = 0;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};

    len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Looking at %s (#%d)", target->name, target->id);
    if (strlen(target->description))
    {
        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n%s", target->description);
    }
    notify_entity(entity, output_buffer);
}

void do_look_room(GameState *gameState, Entity *entity, Entity *room)
{
    u32 i;
    i32 len = 0;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    b32 hasPlayers = false;
    b32 hasThings = false;

    if (strlen(room->description))
    {
        len += snprintf(output_buffer, MAX_BUFFER_SIZE - len - 1, "%s", room->description);
    }

    // NOTE(marshel): List exits
    for (i = 0; i < dynamic_array_size(room->doors); ++i)
    {
        if (i == 0)
        {
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nExits:");
        }

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n\t%s", room->doors[i].name);
    }

    // NOTE(marshel): List players
    for (i = 0; i < dynamic_array_size(room->contents); ++i)
    {
        Entity *e = entity_search(gameState, room->contents[i]);
        if (e && e->type == EntityType_Player && e->connection)
        {
            if (hasPlayers == false)
            {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nPlayers:");
                hasPlayers = true;
            }
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len -1, "\n\t%s (#%d)", e->name, e->id);
        }
    }

    // NOTE(marshel): List things
    for (i = 0; i < dynamic_array_size(room->contents); ++i)
    {
        Entity *e = entity_search(gameState, room->contents[i]);
        if (e && e->type == EntityType_Thing)
        {
            if (hasThings == false)
            {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nThings:");
                hasPlayers = true;
            }
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len -1, "\n\t%s (#%d)", e->name, e->id);
        }
    }

    notify_entity(entity, output_buffer);
}

void do_look_thing(Entity *entity, Entity *thing)
{
    i32 len = 0;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};

    len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "Looking at %s (#%d)", thing->name, thing->id);
    if (strlen(thing->description))
    {
        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n%s", thing->description);
    }
    notify_entity(entity, output_buffer);
}
