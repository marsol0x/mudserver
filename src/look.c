/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

void do_look_user(Entity *entity, Entity *target)
{
    notify_entity(entity, "Looking at %s(#%d)", target->name.str, target->id);
    if (strlen(target->description))
    {
        notify_entity(entity, "%s", target->description);
    }
}

void do_look_room(GameState *gameState, Entity *entity, Entity *room)
{
    u32 i;
    i32 len = 0;
    u8 output_buffer[MAX_BUFFER_SIZE] = {0};
    b32 hasPlayers = false;
    b32 hasThings = false;

    if (strlen(room->description))
    {
        len += snprintf(output_buffer, MAX_BUFFER_SIZE - len - 1, "%s", room->description);
    }

    // NOTE(marshel): List players
    for (i = 0; i < dynamic_array_size(room->contents); ++i)
    {
        Entity *e = entity_search(gameState, room->contents[i]);
        if (e && e->type == EntityType_Player && e->connection)
        {
            if (hasPlayers == false)
            {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nPlayers:");
                hasPlayers = true;
            }
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len -1, "\n\t%s(#%d)", e->name.str, e->id);
        }
    }

    // NOTE(marshel): List things
    for (i = 0; i < dynamic_array_size(room->contents); ++i)
    {
        Entity *e = entity_search(gameState, room->contents[i]);
        if (e && e->type == EntityType_Thing)
        {
            if (hasThings == false)
            {
                len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nThings:");
                hasThings = true;
            }
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len -1, "\n\t%s(#%d)", e->name.str, e->id);
        }
    }

    // NOTE(marshel): List exits
    for (i = 0; i < dynamic_array_size(room->doors); ++i)
    {
        if (i == 0)
        {
            len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\nExits:");
        }

        len += snprintf(output_buffer + len, MAX_BUFFER_SIZE - len - 1, "\n\t%s", room->doors[i].name.str);
    }

    notify_entity(entity, output_buffer);
}

void do_look_thing(Entity *entity, Entity *thing)
{
    notify_entity(entity, "Looking at %s(#%d)", thing->name.str, thing->id);
    if (strlen(thing->description))
    {
        notify_entity(entity, "%s", thing->description);
    }
}
