/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>

static char * read_entire_file_into_memory(MemoryArena *arena, FILE *saveFile)
{
    char *result = 0;

    fseek(saveFile, 0, SEEK_END);
    u64 saveFileSize = ftell(saveFile);
    fseek(saveFile, 0, SEEK_SET);

    if (saveFileSize)
    {
        result = memory_arena_allocate(arena, (u32)saveFileSize);
        size_t readSize = fread(result, 1, saveFileSize, saveFile);
        if (readSize != saveFileSize)
        {
            result = 0;
        }
    }

    return result;
}

#include "game.h"
#include "parse_savefile.c"

void game_save_entity(MemoryArena *arena, Entity *entity, FILE *saveFile)
{
    fprintf(saveFile, "\t@Entity{");

    fprintf(saveFile, "\n\t\t@Id: %d",            entity->id);
    fprintf(saveFile, "\n\t\t@Location: %d",      entity->location);
    fprintf(saveFile, "\n\t\t@Type: %d",          entity->type);
    fprintf(saveFile, "\n\t\t@Name: %s",          entity->name.str);
    fprintf(saveFile, "\n\t\t@Lastpaged: %d",     entity->lastPaged);

    if (*entity->alias)       fprintf(saveFile, "\n\t\t@Alias: %s",         entity->alias);
    if (*entity->description) fprintf(saveFile, "\n\t\t@Description{%s}",   entity->description);
    if (*entity->password)    fprintf(saveFile, "\n\t\t@Password: %s",      entity->password);

    if (dynamic_array_size(entity->contents))
    {
        fprintf(saveFile, "\n\t\t@Contents{");
        for (u32 index = 0; index < dynamic_array_size(entity->contents); ++index)
        {
            fprintf(saveFile, "\n\t\t\t@: %d", entity->contents[index]);
        }
        fprintf(saveFile, "\n\t\t}");
    }

    if (dynamic_array_size(entity->doors))
    {
        fprintf(saveFile, "\n\t\t@Doors{");
        for (u32 index = 0; index < dynamic_array_size(entity->doors); ++index)
        {
            Door *door = entity->doors + index;
            fprintf(saveFile, "\n\t\t\t@Door{");
            fprintf(saveFile, "\n\t\t\t\t@Name: %s", door->name.str);
            fprintf(saveFile, "\n\t\t\t\t@Destination: %d", door->dest);
            if (dynamic_array_size(door->aliases))
            {
                fprintf(saveFile, "\n\t\t\t\t@Aliases{");
                for (u32 alias_index = 0; alias_index < dynamic_array_size(door->aliases); ++alias_index)
                {
                    String *alias = door->aliases + alias_index;
                    fprintf(saveFile, "\n\t\t\t\t\t@: %s", alias->str);
                }
                fprintf(saveFile, "\n\t\t\t\t}");
            }
            fprintf(saveFile, "\n\t\t\t}");
        }
        fprintf(saveFile, "\n\t\t}");
    }

    fprintf(saveFile, "\n\t}\n");
}

void game_save_entitites(MemoryArena *arena, RBTree *tree, RBNode *entity, FILE *saveFile)
{
    if (entity != tree->sentinel)
    {
        game_save_entitites(arena, tree, entity->left, saveFile);
        game_save_entity(arena, (Entity *)entity, saveFile);
        game_save_entitites(arena, tree, entity->right, saveFile);
    }
}

void game_save_channel(ChatChannel *channel, FILE *saveFile)
{
    u32 index;

    fprintf(saveFile, "\t@Channel{");
    fprintf(saveFile, "\n\t\t@Name: %s", channel->name.str);

    if (dynamic_array_size(channel->members))
    {
        fprintf(saveFile, "\n\t\t@Members{");
        for (index = 0;
             index < dynamic_array_size(channel->members);
             ++index)
        {
            fprintf(saveFile, "\n\t\t\t@: %d", channel->members[index]);
        }
        fprintf(saveFile, "\n\t\t}");
    }
    fprintf(saveFile, "\n\t}\n");
}

void game_save_channels(PrefixTree *tree, FILE *saveFile)
{
    if (tree)
    {
        Queue stack;
        DLIST_INIT(&stack);
        queue_push(&stack, &tree->next, sizeof(&tree->next));

        while (stack.next != &stack)
        {
            PrefixTree *node;
            queue_pop(&stack, &node);

            if (node)
            {
                if (node->data)
                {
                    game_save_channel(node->data, saveFile);
                }

                if (node->child)
                {
                    queue_push(&stack, &node->child, sizeof(&node->child));
                }

                if (node->next)
                {
                    queue_push(&stack, &node->next, sizeof(&node->next));
                }
            }
        }
    }
}

void game_save(GameState *gameState)
{
    char *path = gameState->saveFilePath;
    FILE *saveFile = fopen(path, "wb");

    notify_all_connected(gameState, "Beignning database dump...");
    if (!saveFile)
    {
        fprintf(stderr, "Failed to open file for writing: %s\n", path);
        perror("Failed to open file for writing");
        notify_all_connected(gameState, "Dump failed. See logs.");
    } else {
        //
        // NOTE(marshel): Metadata
        //
        fprintf(saveFile, "@Version: %d\n", VERSION);

        //
        // NOTE(marshel): Entities
        //
        fprintf(saveFile, "@Entities{\n");
        game_save_entitites(&gameState->temporary_memory, gameState->db, gameState->db->root, saveFile); 
        fprintf(saveFile, "}\n");

        //
        // NOTE(marshel): Channels
        //
        fprintf(saveFile, "@Channels{\n");
        game_save_channels(gameState->channels, saveFile);
        fprintf(saveFile, "}\n");

        fclose(saveFile);
        notify_all_connected(gameState, "Dump complete.");
        printf("Dump complete.\n");
    }
}

void game_save_timer(GameState *gameState, void *args)
{
    game_save(gameState);
}

void gamestate_init(GameState *gameState)
{
    gameState->db = xcalloc(1, sizeof(RBTree));
    rb_init_tree(gameState->db);

    gameState->channels = ptree_init();
    gameState->starting_room = 1;
    DLIST_INIT(&gameState->connections);

    init_memory_arena(&gameState->temporary_memory, MEGABYTES(4));
}

static i32 parse_int_string(MemoryArena *arena, String string)
{
    char *str = memory_arena_allocate(arena, string.length + 1);
    strncpy(str, string.str, string.length);
    str[string.length] = 0;
    i32 result = atoi(str);

    return result;
}

#define is_attribute(node, attr) string_compare_insensitive(node->tag.str, attr, strlen(attr)) == 0
static void game_load_entity(GameState *gameState, Node *node)
{
    Entity *entity = entity_init(gameState, -1, EntityType_None);

    for (i32 childIndex = 0;
         childIndex < dynamic_array_size(node->children);
         ++childIndex)
    {
        Node *child = node->children + childIndex;
        if (0) {
        } else if (is_attribute(child, "Id")) {
            entity->id = parse_int_string(&gameState->temporary_memory, child->body);

        } else if (is_attribute(child, "Location")) {
            entity->location = parse_int_string(&gameState->temporary_memory, child->body);

        } else if (is_attribute(child, "Type")) {
            entity->type = parse_int_string(&gameState->temporary_memory, child->body);

        } else if (is_attribute(child, "Name")) {
            entity->name = string_copy(child->body);

        } else if (is_attribute(child, "Lastpaged")) {
            entity->lastPaged = parse_int_string(&gameState->temporary_memory, child->body);

        } else if (is_attribute(child, "Alias")) {
            strncpy(entity->alias, child->body.str, child->body.length);

        } else if (is_attribute(child, "Descripion")) {
            strncpy(entity->description, child->body.str, child->body.length);

        } else if (is_attribute(child, "Password")) {
            strncpy(entity->password, child->body.str, child->body.length);

        } else if (is_attribute(child, "Contents")) {
            for (i32 contentIndex = 0;
                 contentIndex < dynamic_array_size(child->children);
                 ++contentIndex)
            {
                Node *content = child->children + contentIndex;
                dbid contentId = parse_int_string(&gameState->temporary_memory, content->body);
                dynamic_array_push(entity->contents, contentId);
            }

        } else if (is_attribute(child, "Doors")) {
            for (i32 doorIndex = 0;
                 doorIndex < dynamic_array_size(child->children);
                 ++doorIndex)
            {
                Node *doorNode = child->children + doorIndex;
                Door door = {0};
                for (i32 doorFieldIndex = 0;
                     doorFieldIndex < dynamic_array_size(doorNode->children);
                     ++doorFieldIndex)
                {
                    Node *field = doorNode->children + doorFieldIndex;
                    if (0) {
                    } else if (string_compare_insensitive(field->tag.str, "Name", 4)         == 0) {
                        door.name = string_copy(field->body);

                    } else if (string_compare_insensitive(field->tag.str, "Destination", 11) == 0) {
                        door.dest = parse_int_string(&gameState->temporary_memory, field->body);

                    } else if (string_compare_insensitive(field->tag.str, "Aliases", 7)      == 0) {
                        for (i32 doorAliasIndex = 0;
                             doorAliasIndex < dynamic_array_size(field->children);
                             ++doorAliasIndex)
                         {
                             Node *a = field->children + doorAliasIndex;
                             String alias = string_copy(a->body);
                             dynamic_array_push(door.aliases, alias);
                         }
                    }
                }
                dynamic_array_push(entity->doors, door);
            }
         }
    }

    rb_insert(gameState->db, (RBNode *)entity);
}

static void game_load_channel(GameState *gameState, Node *node)
{
    ChatChannel *channel = xcalloc(1, sizeof(*channel));
    for (i32 fieldIndex = 0;
         fieldIndex < dynamic_array_size(node->children);
         ++fieldIndex)
    {
        Node *field = node->children + fieldIndex;
        if (0) {
        } else if (is_attribute(field, "Name")) {
            channel->name = string_copy(field->body);

        } else if (is_attribute(field, "Members")) {
            for (i32 memberIndex = 0;
                 memberIndex < dynamic_array_size(field->children);
                 ++memberIndex)
             {
                 Node *memberField = field->children + memberIndex;
                 dbid memberId = parse_int_string(&gameState->temporary_memory, memberField->body);
                 dynamic_array_push(channel->members, memberId);
             }
        }
    }

    if (channel->name.length)
    {
        char *lowerName = string_to_lowercase(&gameState->temporary_memory, channel->name.str);
        ptree_add(gameState->channels, lowerName, channel);
    } else {
        free(channel);
    }
}

b32 game_load(GameState *gameState)
{
    b32 result = true;

    FILE *saveFile = fopen(gameState->saveFilePath, "rb");
    if (saveFile)
    {
        char *saveFileContents = read_entire_file_into_memory(&gameState->temporary_memory, saveFile);
        if (saveFileContents)
        {
            fclose(saveFile);

            ParserState parserState = {0};
            parserState.stream = saveFileContents;
            dynamic_array_init(parserState.nodes, Node);
            parse_save_data(&parserState);

            if (dynamic_array_size(parserState.nodes))
            {
                for (i32 nodeIndex = 0;
                     nodeIndex < dynamic_array_size(parserState.nodes);
                     ++nodeIndex)
                {
                    Node *node = parserState.nodes + nodeIndex;
                    if (0) {
                    } else if (is_attribute(node, "Version")) {
                        i32 versionInt = parse_int_string(&gameState->temporary_memory, node->body);
                        if (VERSION != versionInt)
                        {
                            printf("Savefile version %d currently unsupported.\n", versionInt);
                            result = false;
                        }
                    } else if (is_attribute(node, "Entities")) {
                        for (i32 nodeIndex = 0;
                             nodeIndex < dynamic_array_size(node->children);
                             ++nodeIndex)
                        {
                            Node *entity = node->children + nodeIndex;
                            game_load_entity(gameState, entity);
                        }
                    } else if (is_attribute(node, "Channels")) {
                        for (i32 nodeIndex = 0;
                             nodeIndex < dynamic_array_size(node->children);
                             ++nodeIndex)
                        {
                            Node *channel = node->children + nodeIndex;
                            game_load_channel(gameState, channel);
                        }
                    } else{
                        printf("Unknown node type: %.*s\n", node->tag.length, node->tag.str);
                        result = false;
                    }
                }
            } else {
                printf("Loading save file failed.\n");
                result = false;
            }
        } else {
            printf("Failed to read entire save file: %s\n", gameState->saveFilePath);
            result = false;
        }
    } else {
        printf("Could not open savefile: %s\n", gameState->saveFilePath);
        result = false;
    }

    return result;
}
#undef is_attribute

void game_initialize(GameState *gameState, char *path)
{
    u32 i;
    Entity *rooms[2];

    char *chans[] = {
        "Admin",
        "Judges",
        "Public",
        "War",
    };

    for (i = 0; i < ARRAYLEN(chans); ++i)
    {
        chat_new_channel(gameState, chans[i]);
    }

    {
        Entity *item;
        Entity *starting_room;

        rooms[0] = entity_create(gameState, -1, EntityType_Room, "Starting Room", "This is the starting room!");
        rooms[1] = entity_create(gameState, -1, EntityType_Room, "Second Room",   "This is the second room!");
        starting_room = rooms[0];
        gameState->starting_room = starting_room->id;

        Door *door;
        door = door_create(rooms[1]->id, "North");
        door_add_alias(door, "N");
        dynamic_array_push(rooms[0]->doors, *door);
        door = door_create(rooms[0]->id, "South");
        door_add_alias(door, "So");
        dynamic_array_push(rooms[1]->doors, *door);

        item = entity_create(gameState, starting_room->id, EntityType_Thing, "Item", "This is an item!\nWith a newline!");
        entity_add_content(gameState, starting_room, item);
    }
}
