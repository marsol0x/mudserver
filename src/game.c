/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>

#include "game.h"
#include "parse_savefile.c"

// NOTE(mashel): Save file format
//
// Game Metadata
// Entities
//  !dbid
//  Key Value
// Channels
//  $name
//  Key Value

char * string_revert_newlines(char *str)
{
    u64 strLength = strlen(str);
    char *result = malloc(strLength * 2);
    char *r;

    r = result;
    while (*str)
    {
        if (*str == '\\')
        {
            if (*(str + 1) == 'n')
            {
                *r++ = '\n';
                str += 2;
            } else {
                *r++ = '\\';
            }
        } else {
            *r++ = *str++;
        }
    }
    *r = 0;

    return result;
}

char * string_convert_newlines(char *str)
{
    char *result, *r;
    u64 i, numNewlines = 0;
    u64 strLength = strlen(str);

    for (i = 0; i < strLength; ++i)
    {
        if (str[i] == '\n')
        {
            ++numNewlines;
        }
    }

    result = malloc(strLength + numNewlines + 1);
    r = result;
    while (*str)
    {
        if (*str == '\n')
        {
            *r++ = '\\';
            *r++ = 'n';
            ++str;
        } else {
            *r++ = *str++;
        }
    }
    *r = 0;

    return result;
}

void game_save_entity(Entity *entity, FILE *saveFile)
{
    u32 index;
    char *desc = 0;
    char *doorName = 0;

    desc = string_convert_newlines(entity->description);
    fprintf(saveFile, "!%d\n",              entity->id);
    fprintf(saveFile, "\tlocation #%d\n",   entity->location);
    fprintf(saveFile, "\ttype %d\n",        entity->type);
    fprintf(saveFile, "\tname %s\n",        entity->name);
    fprintf(saveFile, "\tdescription %s\n", desc);
    fprintf(saveFile, "\tlastpaged #%d\n",  entity->lastPaged);

    fprintf(saveFile, "\tcontents");
    for (index = 0; index < dynamic_array_size(entity->contents); ++index)
    {
        fprintf(saveFile, " #%d", entity->contents[index]);
    }
    fprintf(saveFile, "\n");

    fprintf(saveFile, "\tdoors");
    for (index = 0; index < dynamic_array_size(entity->doors); ++index)
    {
        doorName = string_convert_newlines(entity->doors[index].name);
        fprintf(saveFile, " #%d;%s", entity->doors[index].dest, doorName);
    }
    fprintf(saveFile, "\n");

    free(desc);
    desc = 0;
}

void game_save_entitites(RBTree *tree, RBNode *entity, FILE *saveFile)
{
    if (entity != tree->sentinel)
    {
        game_save_entitites(tree, entity->left, saveFile);
        game_save_entity((Entity *)entity, saveFile);
        game_save_entitites(tree, entity->right, saveFile);
    }
}

void game_save_channel(ChatChannel *channel, FILE *saveFile)
{
    u32 index;

    fprintf(saveFile, "$%s\n", channel->name);
    fprintf(saveFile, "\tmembers ");
    
    for (index = 0;
         index < dynamic_array_size(channel->members);
         ++index)
    {
        fprintf(saveFile, "#%d ", channel->members[index]);
    }
    fprintf(saveFile, "\n");
}

void game_save_channels(PrefixTree *tree, FILE *saveFile)
{
    Queue stack;
    DLIST_INIT(&stack);
    queue_push(&stack, &tree->next, sizeof(&tree->next));

    while (stack.next != &stack)
    {
        PrefixTree *node;
        queue_pop(&stack, &node);

        if (node->data)
        {
            game_save_channel(node->data, saveFile);
        }

        if (node->child)
        {
            queue_push(&stack, &node->child, sizeof(&node->child));
        }

        if (node->next)
        {
            queue_push(&stack, &node->next, sizeof(&node->next));
        }
    }
}

// TODO(marshel): Convert this to use dependency injection
void game_save(GameState *gameState, char *path)
{
    FILE *saveFile = fopen(path, "w");
    if (!saveFile)
    {
        fprintf(stderr, "Failed to open file for writing: %s\n", path);
        perror("Failed to open file for writing");
    } else {
        // TODO(marshel): Metadata
        fprintf(saveFile, "v%d\n", VERSION);
        // TODO(marshel): Entities
        game_save_entitites(gameState->db, gameState->db->root, saveFile); 

        // TODO(marshel): Channels
        game_save_channels(gameState->channels, saveFile);

        fclose(saveFile);
    }
}

b32 game_check_version(SaveData *saveData)
{
    return VERSION == saveData->version;
}

b32 game_load_entities(GameState *gameState, SaveData *saveData)
{
    b32 result = false;
    ObjectData *entity;

    for (entity = saveData->entities.next;
         entity != &saveData->entities;
         entity = entity->next)
    {
        Entity *databaseEntity;
        KeyValuePair *attribute;

        databaseEntity = entity_init(gameState, -1, EntityType_None);
        databaseEntity->id = entity->header.id;

        for (attribute = entity->attributes.next;
             attribute != &entity->attributes;
             attribute = attribute->next)
        {
            if (string_compare_insensitive("location", attribute->key, 8) == 0)
            {
                char *locationString = attribute->value + 1; // NOTE(marshel): Skip the '#'
                databaseEntity->location = atoi(locationString);
            } else if (string_compare_insensitive("type", attribute->key, 4) == 0) {
                char *typeString = attribute->value;
                databaseEntity->type = atoi(typeString);
            } else if (string_compare_insensitive("name", attribute->key, 4) == 0) {
                strcpy(databaseEntity->name, attribute->value);
            } else if (string_compare_insensitive("description", attribute->key, 11) == 0) {
                if (attribute->value)
                {
                    char *description = string_revert_newlines(attribute->value);
                    strcpy(databaseEntity->description, description);
                    free(description);
                }
            } else if (string_compare_insensitive("lastpaged", attribute->key, 9) == 0) {
                if (attribute->value)
                {
                    char *lastpagedString = attribute->value;
                    databaseEntity->lastPaged = atoi(lastpagedString);
                } else {
                    databaseEntity->lastPaged = -1;
                }
            } else if (string_compare_insensitive("contents", attribute->key, 8) == 0) {
                if (attribute->value)
                {
                    char dbidString[16] = {0};
                    dbid dbId;
                    char *index = attribute->value;
                    while (*index)
                    {
                        index += eat_all_whitespace(index, 0);
                        if (*index == '#')
                        {
                            char *p = index;
                            while (*p && !is_whitespace(*p))
                            {
                                ++p;
                            }

                            *p = 0;
                            ++index;
                            strcpy(dbidString, index);
                            dbId = atoi(dbidString);
                            dynamic_array_push(databaseEntity->contents, dbId);

                            index = ++p;
                        }
                    }
                }
            } else if (string_compare_insensitive("doors", attribute->key, 5) == 0) {
                if (attribute->value)
                {
                    char doorDestString[16] = {0};
                    dbid doorDest;
                    char *index = attribute->value;
                    while (*index)
                    {
                        Door *door;
                        index += eat_all_whitespace(index, 0);
                        if (*index == '#')
                        {
                            char *p = index;
                            while (*p && *p != ';')
                            {
                                ++p;
                            }
                            *p = 0;
                            ++index;
                            strcpy(doorDestString, index);
                            doorDest = atoi(doorDestString);
                            index = ++p;

                            while (*p && !is_whitespace(*p))
                            {
                                ++p;
                            }
                            *p = 0;

                            door = door_create(doorDest, index);
                            dynamic_array_push(databaseEntity->doors, *door);

                            index = ++p;
                        }
                    }
                }
            }
        }

        rb_insert(gameState->db, (RBNode *)databaseEntity);
        result = true;
    }

    return result;
}

b32 game_load_channels(GameState *gameState, SaveData *saveData)
{
    b32 result = false;
    ObjectData *channel;

    for (channel = saveData->channels.next;
            channel != &saveData->channels;
            channel = channel->next)
    {
        ChatChannel *newChannel = chat_new_channel(gameState, channel->header.name);
        if (newChannel)
        {
            KeyValuePair *attribute;
            for (attribute = channel->attributes.next;
                    attribute != &channel->attributes;
                    attribute = attribute->next)
            {
                if (string_compare_insensitive("members", attribute->key, 7) == 0)
                {
                    if (attribute->value)
                    {
                        char dbidString[16] = {0};
                        dbid dbId;
                        char *index = attribute->value;
                        while (*index)
                        {
                            index += eat_all_whitespace(index, 0);
                            if (*index == '#')
                            {
                                char *p = index;
                                while (!is_whitespace(*p))
                                {
                                    ++p;
                                }

                                *p = 0;
                                ++index;
                                strcpy(dbidString, index);
                                dbId = atoi(dbidString);
                                channel_add_member_by_id(newChannel, dbId);

                                index = ++p;
                            }
                        }
                    }
                }
            }
            result = true;
        }
    }

    return result;
}

void gamestate_init(GameState *gameState)
{
    gameState->db = malloc(sizeof(*gameState->db));
    rb_init_tree(gameState->db);

    gameState->channels = ptree_init();

    gameState->connections = malloc(sizeof(*gameState->connections));
    DLIST_INIT(gameState->connections);
}

// TODO(marshel): Convert this to use dependency injection
b32 game_load(GameState *gameState, char *path)
{
    b32 result = false;
    FILE *loadFile = fopen(path, "r");

    if (loadFile)
    {
        char *saveFileContents = read_entire_file_into_memory(loadFile);
        if (saveFileContents)
        {
            SaveData saveData;
            b32 isCorrectVersion = false;
            b32 entitiesLoaded = false;
            b32 channelsLoaded = false;

            save_data_init(&saveData);
            parse_save_file(saveFileContents, &saveData);

            gamestate_init(gameState);

            isCorrectVersion = game_check_version(&saveData);
            entitiesLoaded = game_load_entities(gameState, &saveData);
            channelsLoaded = game_load_channels(gameState, &saveData);

            if (isCorrectVersion && entitiesLoaded && channelsLoaded)
            {
                result = true;
            }
        }

        fclose(loadFile);
    } else {
        if (errno != ENOENT)
        {
            fprintf(stderr, "Failed to open file for reading: %s\n", path);
            perror("Failed to open file for reading");
        }
    }

    return result;
}

void game_initialize(GameState *gameState, char *path)
{
    u32 i;
    Entity *rooms[2];

    char *chans[] = {
        "Admin",
        "Judges",
        "Public",
        "War",
    };

    gamestate_init(gameState);

    for (i = 0; i < ARRAYLEN(chans); ++i)
    {
        chat_new_channel(gameState, chans[i]);
    }

    {
        Entity *item;

        rooms[0] = entity_create(gameState, -1, EntityType_Room, "Starting Room", "This is the starting room!");
        rooms[1] = entity_create(gameState, -1, EntityType_Room, "Second Room",   "This is the second room!");
        gameState->starting_room = rooms[0];

        dynamic_array_push(rooms[0]->doors, *door_create(rooms[1]->id, "North"));
        dynamic_array_push(rooms[1]->doors, *door_create(rooms[0]->id, "South"));

        item = entity_create(gameState, gameState->starting_room->id, EntityType_Thing, "Item", "This is an item!\nWith a newline!");
        entity_add_content(gameState, gameState->starting_room, item);
    }
}
