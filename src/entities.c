/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

Entity * entity_init(GameState *gameState, dbid location, u8 type)
{
    Entity *e = malloc(sizeof(*e));
    memset(e, 0, sizeof(*e));
    e->type     = type;
    e->location = location;
    rb_init(gameState->db, (RBNode *)e);

    dynamic_array_init(e->contents, dbid);
    dynamic_array_init(e->doors,    Door);

    e->lastPaged = -1;
    if (type == EntityType_Room)
    {
        e->location = -1;
    }

    return e;
}

Entity * entity_create(GameState *gameState, dbid location, u8 type, char *name, char *description)
{
    Entity *result = entity_init(gameState, location, type);
    if (name)
    {
        memcpy(result->name, name, strlen(name));
    }

    if (description)
    {
        memcpy(result->description, description, strlen(description));
    }

    rb_insert(gameState->db, (RBNode *)result);

    return result;
}

#define entity_copy(gameState, entity) entity_create(gameState, entity->location, entity->type, entity->name, entity->description)

void entity_delete(GameState *gameState, Entity *entity)
{
    rb_delete(gameState->db, (RBNode *)entity);

    // TODO(marshel): Determine what happens to an entity's contents when that
    // entity is deleted
#if 0
    e = entity->contents->next;
    while (e != entity->contents)
    {
        next = e->next;
        if (e->type == EntityType_Player)
        {
            e->location = gameState->startingRoom->id;
            DLIST_REMOVE(e);
            DLIST_ADD(gameState->startingRoom, e);
        } else {
            entity_delete(gameState, e);
        }
        e = next;
    }
    free(entity->contents);
#else
    dynamic_array_free(entity->contents);
    dynamic_array_free(entity->doors);
#endif

    // TODO(marshel): Get list of channels an entity is part of and then remove
    // membership from each

    free(entity);
}

Entity * entity_find_by_name(RBTree *tree, u8 *name)
{
    Entity *e = (Entity *)tree->root;
    i8 found = 0;
    Queue stack;
    DLIST_INIT(&stack);
    queue_push(&stack, &e, sizeof(&e));
    while (stack.next != &stack)
    {
        queue_pop(&stack, &e); 

        if (string_compare_insensitive(e->name, name, MAX_NAME_SIZE) == 0)
        {
            found = 1;
            break;
        }

        if (e->left && e->left != tree->sentinel)
        {
            queue_push(&stack, &e->left, sizeof(&e->left));
        }

        if (e->right && e->right != tree->sentinel)
        {
            queue_push(&stack, &e->right, sizeof(&e->right));
        }
    }

    return found ? e : 0;
}

u32 entity_disconnect(GameState *gameState, Entity *entity)
{
    u32 new_nfds;
    Connection *conn = 0;
    for (conn = gameState->connections->next; conn != gameState->connections; conn = conn->next)
    {
        if (conn->entity == entity)
        {
            break;
        }
    }

    if (conn != gameState->connections)
    {
        if (gameState->nfds == conn->sock)
        {
            new_nfds = gameState->nfds - 1;
        }

        if (strlen(entity->name) > 0)
        {
            u8 buf[MAX_BUFFER_SIZE];
            i32 count = snprintf(buf, MAX_BUFFER_SIZE - 1, "%s has disconnected.", entity->name);
            Entity *loc = entity_search(gameState, entity->location);
            assert(loc);

            entity->connection->isConnected = false;
            free(entity->connection);
            entity->connection = 0;
            buf[count] = 0;
            queue_push(&gameState->output_queue, (void *) buf, (u32)count);
        }

        // TODO(marshel): Make sure we clear out *all* connected references to a player
        printf("Connection disconnect: %s (%d)\n", *entity->name ? (char *)entity->name : "", conn->sock);
        closesocket(conn->sock);
        FD_CLR(conn->sock, &gameState->masterfds);
    }

    // TODO(marshel): This is a bad place for this
    {
        if (gameState->nfds == conn->sock)
        {
            for (new_nfds = gameState->nfds; new_nfds > 2; --new_nfds)
            {
                if (FD_ISSET(new_nfds, &gameState->masterfds))
                {
                    break;
                }
            }
        } else {
            new_nfds = gameState->nfds;
        }
    }

    return new_nfds;
}

Entity * find_content_by_name(GameState *gameState, dbid *contents, u8 *name)
{
    Entity *result = 0;
    u32 index;
    b32 found = false;
    for (index = 0; index < dynamic_array_size(contents); ++index)
    {
        result = entity_search(gameState, contents[index]);
        if (string_compare_insensitive(name, result->name, MAX_NAME_SIZE) == 0)
        {
            found = true;
            break;
        }
    }

    return found ? result : 0;
}

#define TOKEN_LOCATION "here"
#define TOKEN_SELF "me"

// TODO(marshel): Split this command up so we can do lookups against things in
// specific locations
Entity * entity_get_from_string(GameState *gameState, Entity *entity, u8 *str)
{
    Entity *result = 0;

    if (str)
    {
        u8 *s = malloc(strlen(str) + 1);
        memcpy(s, str, strlen(str) + 1);

        if (string_compare_insensitive(s, TOKEN_LOCATION, strlen(s)) == 0)
        {
            result = entity_search(gameState, entity->location);
            assert(result);
        } else if (string_compare_insensitive(s, TOKEN_SELF, strlen(s)) == 0) {
            result = entity;
        } else {
            if (*s == TOKEN_NUMBER)
            {
                // NOTE(marshel): Entity lookup by dbid
                dbid id = (dbid)atoi(s + 1);
                result = entity_search(gameState, id);
            } else if (*s == TOKEN_LOOKUP) {
                // NOTE(marshel): Entity lookup by name
                result = entity_find_by_name(gameState->db, s + 1);
            } else {
                // NOTE(marshel): Local entity lookup by name (entity contents or room)
                result = find_content_by_name(gameState, entity->contents, str);
                if (!result)
                {
                    Entity *loc = entity_search(gameState, entity->location);
                    assert(loc);
                    result = find_content_by_name(gameState, loc->contents, str);
                }
            }
        }
        free(s);
    }

    return result;
}

void entity_add_content(GameState *gameState, Entity *target, Entity *object)
{
    Entity *objectLocation;
    char outputBuffer[MAX_BUFFER_SIZE];
    assert(target);
    assert(object);

    objectLocation = entity_search(gameState, object->location);
    if (objectLocation)
    {
        u32 index;
        for (index = 0; index < dynamic_array_size(objectLocation->contents); ++index)
        {
            if (objectLocation->contents[index] == object->id)
            {
                dynamic_array_erase(objectLocation->contents, index);
                break;
            }
        }
    }

    dynamic_array_push(target->contents, object->id);
    object->location = target->id;

    if (objectLocation)
    {
        snprintf(outputBuffer, MAX_BUFFER_SIZE, "%s gave you %s", objectLocation->name, object->name);
        notify_entity(target, "");
    }
}

void entity_drop_content(GameState *gameState, Entity *entity, Entity *object)
{
    Entity *entityLocation;
    char outputBuffer[MAX_BUFFER_SIZE];
    u32 len = 0;
    assert(entity);
    assert(object);

    entityLocation = entity_search(gameState, entity->location);

    if (entity->id == object->location)
    {
        entity_add_content(gameState, entityLocation, object);
        len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "Dropped %s", object->name);
    } else {
        len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "Cannot drop '%s'. You are not holding it.", object->name);
    }
}

Door * door_create(dbid dest, char *name)
{
    Door *result = malloc(sizeof(*result));
    result->dest = dest;
    memcpy(result->name, name, strlen(name) + 1);

    return result;
}
