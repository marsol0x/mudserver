/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

Entity * entity_init(GameState *gameState, dbid location, u8 type)
{
    Entity *e = xcalloc(1, sizeof(Entity));
    e->type     = type;
    e->location = location;
    rb_init(gameState->db, (RBNode *)e);

    dynamic_array_init(e->contents, dbid);
    dynamic_array_init(e->doors,    Door);

    e->lastPaged = -1;
    if (type == EntityType_Room)
    {
        e->location = -1;
    }

    return e;
}

Entity * entity_create(GameState *gameState, dbid location, u8 type, char *name, char *description)
{
    Entity *result = entity_init(gameState, location, type);
    if (name)
    {
        result->name = string_from_cstring(name);
    }

    if (description)
    {
        memcpy(result->description, description, strlen(description));
    }

    rb_insert(gameState->db, (RBNode *)result);

    return result;
}

#define entity_copy(gameState, entity) entity_create(gameState, entity->location, entity->type, entity->name, entity->description)

void entity_delete(GameState *gameState, Entity *entity)
{
    rb_delete(gameState->db, (RBNode *)entity);

    // TODO(marshel): Determine what happens to an entity's contents when that
    // entity is deleted

    dynamic_array_free(entity->contents);
    dynamic_array_free(entity->doors);

    // TODO(marshel): Get list of channels an entity is part of and then remove
    // membership from each

    free(entity);
}

Entity * entity_find_by_name(RBTree *tree, u8 *name)
{
    Entity *e = (Entity *)tree->root;
    i8 found = 0;
    Queue stack;
    DLIST_INIT(&stack);
    queue_push(&stack, &e, sizeof(&e));
    while (stack.next != &stack)
    {
        queue_pop(&stack, &e); 

        if ((string_compare_insensitive(e->name.str, name, e->name.length) == 0) ||
           (string_compare_insensitive(e->alias, name, MAX_BUFFER_SIZE) == 0))
        {
            found = 1;
            break;
        }

        if (e->left && e->left != tree->sentinel)
        {
            queue_push(&stack, &e->left, sizeof(&e->left));
        }

        if (e->right && e->right != tree->sentinel)
        {
            queue_push(&stack, &e->right, sizeof(&e->right));
        }
    }

    return found ? e : 0;
}

void entity_disconnect(GameState *gameState, Entity *entity)
{
    if (entity)
    {
        Connection *conn = 0;
        for (conn = gameState->connections.next; conn != &gameState->connections; conn = conn->next)
        {
            if (conn->entity == entity)
            {
                break;
            }
        }

        if (entity->name.length)
        {
            u8 buf[MAX_BUFFER_SIZE];
            i32 count = snprintf(buf, MAX_BUFFER_SIZE - 1, "%s has disconnected.", entity->name.str);
            Entity *loc = entity_search(gameState, entity->location);
            assert(loc);

            entity->connection->isConnected = false;
            buf[count] = 0;
            queue_push(&gameState->output_queue, (void *) buf, (u32)count);
        }
    }
}

Entity * find_content_by_name(GameState *gameState, dbid *contents, u8 *name)
{
    Entity *result = 0;
    u32 index;
    b32 found = false;
    for (index = 0; index < dynamic_array_size(contents); ++index)
    {
        result = entity_search(gameState, contents[index]);
        if ((string_compare_insensitive(name, result->name.str, result->name.length) == 0) ||
            (string_compare_insensitive(name, result->alias, MAX_NAME_SIZE) == 0))
        {
            found = true;
            break;
        }
    }

    return found ? result : 0;
}

#define TOKEN_LOCATION "here"
#define TOKEN_SELF "me"

// TODO(marshel): Split this command up so we can do lookups against things in
// specific locations
// TODO(marshel): _Require_ location, rather than entity, for this
Entity * entity_get_from_string(GameState *gameState, Entity *entity, u8 *str)
{
    Entity *result = 0;

    if (str)
    {
        u8 *s = memory_arena_allocate(&gameState->temporary_memory, strlen(str) + 1);
        memcpy(s, str, strlen(str) + 1);

        if (string_compare_insensitive(s, TOKEN_LOCATION, strlen(s)) == 0)
        {
            result = entity_search(gameState, entity->location);
            assert(result);
        } else if (string_compare_insensitive(s, TOKEN_SELF, strlen(s)) == 0) {
            result = entity;
        } else {
            if (*s == TOKEN_NUMBER)
            {
                // NOTE(marshel): Entity lookup by dbid
                dbid id = (dbid)atoi(s + 1);
                result = entity_search(gameState, id);
            } else if (*s == TOKEN_LOOKUP) {
                // NOTE(marshel): Entity lookup by name
                result = entity_find_by_name(gameState->db, s + 1);
            } else {
                // NOTE(marshel): Local entity lookup by name (entity contents or room)
                result = find_content_by_name(gameState, entity->contents, str);
                if (!result)
                {
                    Entity *loc = entity_search(gameState, entity->location);
                    assert(loc);
                    result = find_content_by_name(gameState, loc->contents, str);
                }
            }
        }
    }

    return result;
}

i32 entity_sort_contents_by_name_partition(GameState *gameState, dbid *contentsArray, i32 low, i32 high)
{
    dbid temp;
    dbid pivot = contentsArray[high];
    i32 i, j;

    i = low - 1;
    for (j = low; j < high; ++j)
    {
        Entity *contentEntity = entity_search(gameState, contentsArray[j]);
        Entity *pivotEntity = entity_search(gameState, pivot);
        if (string_compare_insensitive(contentEntity->name.str, pivotEntity->name.str, contentEntity->name.length) <= 0)
        {
            ++i;
            temp = contentsArray[i];
            contentsArray[i] = contentsArray[j];
            contentsArray[j] = temp;
        }
    }
    temp = contentsArray[i + 1];
    contentsArray[i + 1] = contentsArray[high];
    contentsArray[high] = temp;

    return i + 1;
}

void entity_sort_contents_by_name(GameState *gameState, dbid *contentsArray, i32 low, i32 high)
{
    if (low < high)
    {
        i32 part = entity_sort_contents_by_name_partition(gameState, contentsArray, low, high);
        entity_sort_contents_by_name(gameState, contentsArray, low, part - 1);
        entity_sort_contents_by_name(gameState, contentsArray, part + 1, high);
    }
}

void entity_add_content(GameState *gameState, Entity *target, Entity *object)
{
    Entity *objectLocation;
    assert(target);
    assert(object);

    objectLocation = entity_search(gameState, object->location);
    if (objectLocation)
    {
        u32 index;
        for (index = 0; index < dynamic_array_size(objectLocation->contents); ++index)
        {
            if (objectLocation->contents[index] == object->id)
            {
                dynamic_array_erase(objectLocation->contents, index);
                break;
            }
        }
    }

    dynamic_array_push(target->contents, object->id);
    object->location = target->id;

    entity_sort_contents_by_name(gameState, target->contents, 0, dynamic_array_size(target->contents) - 1);
}

void entity_drop_content(GameState *gameState, Entity *entity, Entity *object)
{
    Entity *entityLocation;
    char outputBuffer[MAX_BUFFER_SIZE];
    u32 len = 0;
    assert(entity);
    assert(object);

    entityLocation = entity_search(gameState, entity->location);

    if (entity->id == object->location)
    {
        entity_add_content(gameState, entityLocation, object);
        len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "Dropped %s", object->name.str);
    } else {
        len += snprintf(outputBuffer, MAX_BUFFER_SIZE - len, "Cannot drop '%s'. You are not holding it.", object->name.str);
    }
}

void entity_move(GameState *gameState, Entity *target, Entity *dest)
{
    u32 index;
    Entity *loc;

    loc = entity_search(gameState, target->location);

    for (index = 0; index < dynamic_array_size(loc->contents); ++index)
    {
        if (loc->contents[index] == target->id)
        {
            dynamic_array_erase(loc->contents, index);
            break;
        }
    }

    target->location = dest->id;
    dynamic_array_push(dest->contents, target->id);
}

Door * door_create(dbid dest, char *name)
{
    Door *result = xcalloc(1, sizeof(Door));
    u32 name_len = (u32)strlen(name);
    result->dest = dest;
    result->name.str = xmalloc(name_len + 1);
    memcpy(result->name.str, name, name_len);
    result->name.str[name_len] = 0;
    result->name.length = name_len;

    return result;
}

void door_add_alias(Door *door, u8 *alias)
{
    String aliasString = string_from_cstring(alias);
    for (u32 index = 0;
         index < dynamic_array_size(door->aliases);
         ++index)
    {
        String a = door->aliases[index];
        if (string_compare_insensitive(aliasString.str, a.str, aliasString.length) == 0)
        {
            return;
        }
    }

    dynamic_array_push(door->aliases, aliasString);
}

void door_remove_alias(Door *door, u8 *alias)
{
    for (u32 index = 0;
         index < dynamic_array_size(door->aliases);
         ++index)
    {
        String a = door->aliases[index];
        if (string_compare_insensitive(alias, a.str, a.length) == 0)
        {
            dynamic_array_erase(door->aliases, index);
            return;
        }
    }
}
