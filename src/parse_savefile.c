// ==================================================================================
// $File: $
// $Date: $
// $Revision: $
// $Creator: Marshel Helsper $
// $Notice: $
// ==================================================================================
#define DEBUG_PARSE 0
#if DEBUG_PARSE

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define true (1==1)
#define false (!true)

typedef int32_t b32;
typedef int8_t i8; 
typedef int16_t i16;
typedef int32_t i32; 
typedef int64_t i64; 
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef uint32_t dbid;

#include "dynamicarray.c"
#include "memory.c"
#include "string.h"

#endif

typedef enum ParserTokenType
{
    ParserTokenType_Unknown,
    ParserTokenType_EOF,
    ParserTokenType_At,
    ParserTokenType_OpenBrace,
    ParserTokenType_CloseBrace,
    ParserTokenType_OpenParen,
    ParserTokenType_CloseParen,
    ParserTokenType_Equal,
    ParserTokenType_Colon,
    ParserTokenType_Word,
    ParserTokenType_String,
} ParserTokenType;

typedef struct ParserToken
{
    ParserTokenType type;
    char *start;
    char *end;
} ParserToken;

typedef struct Node
{
    String tag;
    String body;
    struct Node *children;
    char *start;
    char *end;
} Node;

typedef struct ParserState
{
    char *stream;
    ParserToken token;
    Node *nodes;
} ParserState;

static void next_token(ParserState *state)
{
    ParserToken result = {0};

    while (*state->stream && !result.type)
    {
        switch (*state->stream)
        {
            case 0:
            {
                result.type = ParserTokenType_EOF;
            } break;

            case '@':
            {
                result.type = ParserTokenType_At;
                result.start = state->stream;
                result.end = ++state->stream;
            } break;

            case '{':
            {
                result.type = ParserTokenType_OpenBrace;
                result.start = state->stream;
                result.end = ++state->stream;
            } break;

            case '}':
            {
                result.type = ParserTokenType_CloseBrace;
                result.start = state->stream;
                result.end = ++state->stream;
            } break;
            
            case '(':
            {
                result.type = ParserTokenType_OpenParen;
                result.start = state->stream;
                result.end = ++state->stream;
            } break;

            case ')':
            {
                result.type = ParserTokenType_CloseParen;
                result.start = state->stream;
                result.end = ++state->stream;
            } break;

            case '=':
            {
                result.type = ParserTokenType_Equal;
                result.start = state->stream;
                result.end = ++state->stream;
            } break;

            case ':':
            {
                result.type = ParserTokenType_Colon;
                result.start = state->stream;
                result.end = ++state->stream;
            } break;

            default:
            {
                ++state->stream;
            } break;
        }
    }

    state->token = result;
}

static String parse_tag(ParserState *state)
{
    String result = {0};
    result.str = state->stream;

    while (*state->stream && !is_whitespace(*state->stream))
    {
        char c = *state->stream;
        if ((c == '{') || (c == '=') || (c == ':'))
        {
             break;
        }
        ++state->stream;
    }
    result.length = (i32)(state->stream - (char *)result.str);

    return result;
}

static void parse_node(ParserState *state, Node *parent)
{
    Node node = {0};

    node.start = state->stream;
    node.tag = parse_tag(state);

    next_token(state);
    switch (state->token.type)
    {
        case ParserTokenType_OpenBrace:
        {
            node.body.str = state->stream;
            i32 braces = 1;
            while (*state->stream == '{')
            {
                ++state->stream;
                ++braces;
            }

            while (*state->stream && braces)
            {
                next_token(state);
                switch (state->token.type)
                {
                    case ParserTokenType_At:
                    {
                        if (*state->token.start && (*(state->token.start - 1) == '\\'))
                        {
                            // NOTE(marshel): Escaped ParserTokenType_At
                            continue;
                        }

                        if (!node.children)
                        {
                            dynamic_array_init(node.children, Node);
                        }
                        parse_node(state, node.children);
                    } break;

                    case ParserTokenType_OpenBrace:
                    {
                        ++braces;
                    } break;

                    case ParserTokenType_CloseBrace:
                    {
                        --braces;
                    } break;

                    default:
                    {
                        // NOTE(marshel): Do nothing, allow next_token to
                        // progress stream
                    } break;
                }
            }
            node.body.length = (i32)(state->stream - (char *)node.body.str - 1);
        } break;

        case ParserTokenType_Equal:
        {
            node.body.str = state->stream;
            while (*state->stream && !is_whitespace(*state->stream))
            {
                char c = *state->stream;
                if (c == '}')
                {
                    break;
                }
                ++state->stream;
            }
            node.body.length = (i32)(state->stream - (char *)node.body.str - 1);
        } break;

        case ParserTokenType_Colon:
        {
            // NOTE(marshel): Strip leading whitespace
            while (*state->stream && is_whitespace(*state->stream))
            {
                ++state->stream;
            }

            node.body.str = state->stream;
            while (*state->stream && !is_end_of_line(*state->stream))
            {
                ++state->stream;
            }
            node.body.length = (i32)(state->stream - (char *)node.body.str);
        } break;

        default:
        {
            assert(!"Invalid token type while parsing a node.");
        } break;
    }
    node.end = state->stream;
    dynamic_array_push(parent, node);
}

static void parse_save_data(ParserState *state)
{
    next_token(state);
    while (*state->stream)
    {
        switch (state->token.type)
        {
            case ParserTokenType_At:
            {
                parse_node(state, state->nodes);
            } break;

            default:
            {
                next_token(state);
            } break;
        }
    }
}

#if DEBUG_PARSE
static void print_node(Node *node, i32 indent)
{
    i32 i = indent;
    while (i--) { putchar('\t'); }
    printf("@%.*s\n", (i32)node->tag.length, node->tag.str);

    if (node->children && dynamic_array_size(node->children))
    {
        for (i32 childIndex = 0;
             childIndex < dynamic_array_size(node->children);
             ++childIndex)
        {
            Node *child = node->children + childIndex;
            print_node(child, indent + 1);
        }
    }

}

int main(int argc, char **args)
{
    char *saveFilePath = "mudserver.save";
    FILE *saveFile = fopen(saveFilePath, "r");
    if (saveFile)
    {
        MemoryArena arena = {0};
        init_memory_arena(&arena, MEGABYTES(4));

        char *saveFileContents = read_entire_file_into_memory(&arena, saveFile);
        if (saveFileContents)
        {
            fclose(saveFile);

            ParserState parserState = {0};
            parserState.stream = saveFileContents;
            dynamic_array_init(parserState.nodes, Node);
            parse_save_data(&parserState);

            for (i32 nodeIndex = 0;
                 nodeIndex < dynamic_array_size(parserState.nodes);
                 ++nodeIndex)
            {
                Node *node = parserState.nodes + nodeIndex;
                print_node(node, 0);
            }
        } else {
            fprintf(stderr, "Failed to read entire save file.\n");
            exit(2);
        }
    } else {
        perror("Could not open save file.");
        exit(3);
    }

    return 0;
}
#endif
