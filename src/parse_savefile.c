// ==================================================================================
// $File: $
// $Date: $
// $Revision: $
// $Creator: Marshel Helsper $
// $Notice: $
// ==================================================================================
#define DEBUG_PARSE 0
#if DEBUG_PARSE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int b32;
typedef int i32;
typedef int dbid;
#define true (1==1)
#define false (!true)

#define DLIST_INIT(sentinal) \
    (sentinal)->next = (sentinal); \
    (sentinal)->prev = (sentinal)

#define DLIST_ADD(sentinal, node) \
    (node)->next = (sentinal)->next; \
    (node)->prev = (sentinal); \
    (sentinal)->next->prev = (node); \
    (sentinal)->next = (node)

#define DLIST_REMOVE(node) \
    (node)->prev->next = (node)->next; \
    (node)->next->prev = (node)->prev
#endif

typedef struct key_value_pair_t
{
    struct key_value_pair_t *next, *prev;

    char *key;
    char *value;
} KeyValuePair;

enum
{
    OBJECT_TYPE_NONE,

    OBJECT_TYPE_ENTITY,
    OBJECT_TYPE_CHANNEL
};

typedef struct object_data_t
{
    struct object_data_t *next, *prev;

    int type;
    union
    {
        dbid id;
        char *name;
    } header;
    KeyValuePair attributes;
} ObjectData;

typedef struct save_data_t
{
    i32 version;
    ObjectData entities;
    ObjectData channels;
} SaveData;

static char * read_entire_file_into_memory(FILE *file)
{
    assert(file);
    char *result = 0;
    size_t fileSize;

    fseek(file, 0, SEEK_END);
    fileSize = ftell(file);
    fseek(file, 0, SEEK_SET);

    result = (char *)malloc(fileSize + 1);
    fread(result, fileSize, 1, file);
    result[fileSize] = 0;

    return result;
}

static size_t eat_all_whitespace(char *content, size_t filePosition)
{
    while (is_whitespace(content[filePosition]))
    {
        ++filePosition;
    }

    return filePosition;
}

static inline size_t one_past_first_whitespace(char *content, size_t position)
{
    while (!is_whitespace(content[position++]))
        ;

    return position;
}

static inline size_t one_past_end_of_line(char *content, size_t position)
{
    while (!is_end_of_line(content[position++]))
        ;

    return position;
}

static size_t parse_file_version(char *content, size_t filePosition, SaveData *saveData)
{
    filePosition = eat_all_whitespace(content, filePosition);
    size_t endOfLine = one_past_end_of_line(content, filePosition);

    // NOTE(marshel): First character for the version string should be a 'v'
    if (content[filePosition] == 'v')
    {
        ++filePosition;
        size_t versionSize = endOfLine - filePosition - 1;
        char *version = (char *)malloc(versionSize);
        strncpy(version, content + filePosition, versionSize);
        version[versionSize] = 0;

        saveData->version = atoi(version);
    } else {
        saveData->version = -1;
    }

    filePosition = endOfLine;

    return filePosition;
}

static size_t parse_single_entity(char *content, size_t filePosition, ObjectData *entities)
{
    size_t currentPosition = eat_all_whitespace(content, filePosition);
    ObjectData *currentEntity = 0;

    b32 parsingEntity = true;
    while (parsingEntity)
    {
        switch (content[currentPosition])
        {
            case '!':
            {
                // TODO(marshel): Beginning of entity entry
                size_t startPosition = ++currentPosition;
                currentPosition = one_past_end_of_line(content, currentPosition);

                size_t lineSize = currentPosition - startPosition;
                char *line = (char *)malloc(lineSize);
                strncpy(line, content + startPosition, lineSize - 1);
                line[lineSize] = 0;

                ObjectData *newEntity = (ObjectData *)malloc(sizeof(*newEntity));
                newEntity->type = OBJECT_TYPE_ENTITY;
                DLIST_INIT(&newEntity->attributes);

                newEntity->header.id = atoi(line);

                DLIST_ADD(entities, newEntity);
                currentEntity = newEntity;
            } break;

            case '\t':
            {
                assert(currentEntity);

                // TODO(marshel): Attribute
                KeyValuePair *attribute = (KeyValuePair *)malloc(sizeof(*attribute));
                memset(attribute, 0, sizeof(*attribute));
                size_t startPosition;

                ++currentPosition;

                // NOTE(marshel): Get key
                startPosition = currentPosition;
                currentPosition = one_past_first_whitespace(content, currentPosition);
                size_t keySize = currentPosition - startPosition;
                attribute->key = (char *)malloc(keySize);
                strncpy(attribute->key, content + startPosition, keySize - 1);
                attribute->key[keySize] = 0;

                // NOTE(marshel): Get value
                // NOTE(marshel): If the whitespace we hit above is a newline,
                // then this attribute has no value
                if (!is_end_of_line(*(content + currentPosition - 1)))
                {
                    size_t postWhiteSpace;

                    startPosition = currentPosition;
                    currentPosition = one_past_end_of_line(content, currentPosition);

                    postWhiteSpace = eat_all_whitespace(content, startPosition);
                    if (postWhiteSpace >= currentPosition)
                    {
                        attribute->value = 0;
                    } else {
                        size_t valueSize = currentPosition - startPosition;
                        attribute->value = (char *)malloc(valueSize);
                        strncpy(attribute->value, content + startPosition, valueSize - 1);
                        attribute->value[valueSize] = 0;
                    }
                }

                DLIST_ADD(&currentEntity->attributes, attribute);
            } break;

            default:
            {
                // NOTE(marshel): We are no longer parsing an entity
                parsingEntity = false;
            } break;
        }
    }

    return currentPosition;
}

static size_t parse_entities(char *content, size_t filePosition, SaveData *saveData)
{
    while (content[filePosition] != 0)
    {
        size_t oldPosition = filePosition;
        size_t newPosition = parse_single_entity(content, filePosition, &saveData->entities);

        if (newPosition == oldPosition)
        {
            break;
        }

        filePosition = newPosition;
    }

    return filePosition;
}

static size_t parse_single_channel(char *content, size_t filePosition, ObjectData *channels)
{
    size_t currentPosition = eat_all_whitespace(content, filePosition);
    ObjectData *currentChannel = 0;

    b32 parsingChannel = true;
    while (parsingChannel)
    {
        switch (content[currentPosition])
        {
            case '$':
            {
                // TODO(marshel): Beginning of channel entry
                size_t startPosition = ++currentPosition;
                currentPosition = one_past_end_of_line(content, currentPosition);

                size_t lineSize = currentPosition - startPosition;
                char *line = (char *)malloc(lineSize);
                strncpy(line, content + startPosition, lineSize - 1);
                line[lineSize] = 0;

                ObjectData *newChannel = (ObjectData *)malloc(sizeof(*newChannel));
                newChannel->type = OBJECT_TYPE_CHANNEL;
                DLIST_INIT(&newChannel->attributes);

                newChannel->header.name = line;

                DLIST_ADD(channels, newChannel);
                currentChannel = newChannel;
            } break;

            case '\t':
            {
                assert(currentChannel);

                // TODO(marshel): Attribute
                KeyValuePair *attribute = (KeyValuePair *)malloc(sizeof(*attribute));
                memset(attribute, 0, sizeof(*attribute));
                size_t startPosition;

                ++currentPosition;

                // NOTE(marshel): Get key
                startPosition = currentPosition;
                currentPosition = one_past_first_whitespace(content, currentPosition);
                size_t keySize = currentPosition - startPosition;
                attribute->key = (char *)malloc(keySize);
                strncpy(attribute->key, content + startPosition, keySize - 1);
                attribute->key[keySize] = 0;

                // NOTE(marshel): Get value
                // NOTE(marshel): If the whitespace we hit above is a newline,
                // then this attribute has no value
                if (!is_end_of_line(*(content + currentPosition - 1)))
                {
                    size_t postWhiteSpace;

                    startPosition = currentPosition;
                    currentPosition = one_past_end_of_line(content, currentPosition);

                    postWhiteSpace = eat_all_whitespace(content, startPosition);
                    if (postWhiteSpace >= currentPosition)
                    {
                        attribute->value = 0;
                    } else {
                        size_t valueSize = currentPosition - startPosition;
                        attribute->value = (char *)malloc(valueSize);
                        strncpy(attribute->value, content + startPosition, valueSize - 1);
                        attribute->value[valueSize] = 0;
                    }
                }

                DLIST_ADD(&currentChannel->attributes, attribute);
            } break;

            default:
            {
                // NOTE(marshel): We are no longer parsing a channel
                parsingChannel = false;
            } break;
        }
    }

    return currentPosition;
}

static size_t parse_channels(char *content, size_t filePosition, SaveData *saveData)
{
    while (content[filePosition] != 0)
    {
        size_t oldPosition = filePosition;
        size_t newPosition = parse_single_channel(content, filePosition, &saveData->channels);

        if (newPosition == oldPosition)
        {
            break;
        }

        filePosition = newPosition;
    }

    return filePosition;
}

void save_data_init(SaveData *saveData)
{
    memset(saveData, 0, sizeof(*saveData));
    DLIST_INIT(&(saveData->entities));
    DLIST_INIT(&(saveData->channels));
}

void parse_save_file(char *content, SaveData *saveData)
{
    size_t filePosition = 0;

    filePosition = parse_file_version(content, filePosition, saveData);
    filePosition = parse_entities(content, filePosition, saveData);
    filePosition = parse_channels(content, filePosition, saveData);
}

#if DEBUG_PARSE
int main(int ArgCount, char **Args)
{
    if (ArgCount != 2)
    {
        fprintf(stderr, "Invalid number of parameters.\n");
        exit(1);
    }

    char *SaveFilePath = Args[1];
    FILE *SaveFile = fopen(SaveFilePath, "r");
    if (SaveFile)
    {
        char *SaveFileContents = ReadEntireFileIntoMemory(SaveFile);
        if (SaveFileContents)
        {
            fclose(SaveFile);

            save_data SaveData;

            SaveDataInit(&SaveData);
            ParseSaveFile(SaveFileContents, &SaveData);

            printf("Version: %d\n", SaveData.Version);
            for (object_data *Entity = SaveData.Entities.next;
                 Entity != &SaveData.Entities;
                 Entity = Entity->next)
            {
                printf("#%d\n", Entity->Header.Id);
                for (key_value_pair *Attribute = Entity->Attributes.next;
                     Attribute != &Entity->Attributes;
                     Attribute = Attribute->next)
                {
                    printf("\t%s = %s\n", Attribute->Key, Attribute->Value);
                }
            }

            for (object_data *Channel = SaveData.Channels.next;
                 Channel != &SaveData.Channels;
                 Channel = Channel->next)
            {
                printf("%s\n", Channel->Header.Name);
                for (key_value_pair *Attribute = Channel->Attributes.next;
                     Attribute != &Channel->Attributes;
                     Attribute = Attribute->next)
                {
                    printf("\t%s = %s\n", Attribute->Key, Attribute->Value);
                }
            }

        } else {
            fprintf(stderr, "Failed to read entire save file.\n");
            exit(2);
        }
    } else {
        perror("Could not open save file.");
        exit(3);
    }

    return 0;
}
#endif
