/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

Entity * player_create(GameState *gameState, Connection *conn, Entity *location, u8 *name, u8 *password)
{
    Entity *player = entity_create(gameState, gameState->starting_room, EntityType_Player, name, 0);
    conn->entity = player;
    player->connection = conn;
    strncpy(player->password, password, MAX_PASSWORD_SIZE - 1);

    dynamic_array_push(location->contents, player->id);
    do_look_room(gameState, player, location);

    return player;
}

void player_connect(GameState *gameState, Connection *conn, Entity *entity)
{
    Entity *location = entity_search(gameState, entity->location);
    if (!location)
    {
        entity->location = gameState->starting_room;
        location = entity_search(gameState, gameState->starting_room);
    }
    assert(location);

    conn->entity = entity;
    entity->connection = conn;
    conn->isConnected = true;
    do_look_room(gameState, entity, location);
}
