/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include "channels.h"

// TODO(marshel): Clean up function names to be more consistent

b32 channel_is_member(ChatChannel *channel, dbid id)
{
    b32 result = false;
    u32 index;
    for (index = 0;
         index < dynamic_array_size(channel->members);
         ++index)
    {
        if (channel->members[index] == id)
        {
            result = true;
            break;
        }
    }

    return result;
}

ChatChannel * chat_new_channel(GameState *gameState, char *name)
{
    PrefixTree *c = ptree_find(gameState->channels, name);
    ChatChannel *channel = 0;

    if (!c)
    {
        char *lowerName;
        channel = xcalloc(1, sizeof(ChatChannel));
        dynamic_array_init(channel->members, dbid);
        channel->name = string_from_cstring(name);

        lowerName = string_to_lowercase(&gameState->temporary_memory, name);
        ptree_add(gameState->channels, lowerName, channel);
    }

    return channel;
}

ChatChannel * chat_get_channel(GameState *gameState, char *name)
{
    PrefixTree *node;
    ChatChannel *result = 0;
    char *lowerName = string_to_lowercase(&gameState->temporary_memory, name);

    node = ptree_find_nearest(gameState->channels, lowerName);
    if (node)
    {
        result = (ChatChannel *)node->data;
    }

    return result;
}

void channel_add_member_by_id(ChatChannel *channel, dbid id)
{
    if (!channel_is_member(channel, id))
    {
        dynamic_array_push(channel->members, id);
    }
}

void channel_add_member(GameState *gameState, ChatChannel *channel, Entity *entity)
{
    if (!channel_is_member(channel, entity->id))
    {
        channel_add_member_by_id(channel, entity->id);
        notify_channel(gameState, channel, "%s joined the channel.", entity->name.str);
    }
}

void channel_remove_member(GameState *gameState, ChatChannel *channel, Entity *entity)
{
    u32 index;
    for (index = 0;
         index < dynamic_array_size(channel->members);
         ++index)
    {
        if (channel->members[index] == entity->id)
        {
            dynamic_array_erase(channel->members, index);
            break;
        }
    }

    notify_channel(gameState, channel, "%s left the channel.", entity->name.str);
    notify_entity(entity, "You have been removed from channel %s.", channel->name.str);
}

String * channel_membership_list(GameState *gameState, Entity *entity)
{
    String *channelList = 0;
    PrefixTree *channelTree = gameState->channels;

    if (channelTree->next)
    {
        Queue stack;

        dynamic_array_init(channelList, String);

        DLIST_INIT(&stack);
        queue_push(&stack, &channelTree->next, sizeof(&channelTree->next));

        while (stack.next != &stack)
        {
            PrefixTree *node;
            queue_pop(&stack, &node);

            if (node->data)
            {
                ChatChannel *channel = (ChatChannel *)node->data;
                if (channel_is_member(channel, entity->id))
                {
                    dynamic_array_push(channelList, channel->name);
                }
            }

            if (node->child)
            {
                queue_push(&stack, &node->child, sizeof(&node->child));
            }

            if (node->next)
            {
                queue_push(&stack, &node->next, sizeof(&node->next));
            }
        }

        if (!dynamic_array_size(channelList))
        {
            dynamic_array_free(channelList);
            channelList = 0;
        }
    }

    return channelList;
}

void channel_message(GameState *gameState, Entity *entity, char *channelName, char *message)
{
    PrefixTree *node;
    ChatChannel *channel = 0;
    char *lowerChannelName = 0;

    lowerChannelName = string_to_lowercase(&gameState->temporary_memory, channelName);
    node = ptree_find_nearest(gameState->channels, lowerChannelName);
    if (node)
    {
        channel = (ChatChannel *)node->data;
    }

    if (channel)
    {
        if (channel_is_member(channel, entity->id))
        {
            if (message[0] == TOKEN_POSE)
            {
                notify_channel(gameState, channel, "%s %s", entity->name.str, message + 1);
            } else {
                notify_channel(gameState, channel, "%s says, \"%s\"", entity->name.str, message);
            }
        } else {
            notify_entity(entity, "You are not a member of %s.", channel->name.str);
        }
    } else {
        notify_entity(entity, "No such channel: %s.", channelName);
    }
}
