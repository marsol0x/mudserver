/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include "channels.h"

// TODO(marshel): Clean up function names to be more consistent

b32 channel_is_member(ChatChannel *channel, dbid id)
{
    b32 result = false;
    u32 index;
    for (index = 0;
         index < dynamic_array_size(channel->members);
         ++index)
    {
        if (channel->members[index] == id)
        {
            result = true;
            break;
        }
    }

    return result;
}

char * string_to_lowercase(char *str)
{
    u64 i;
    u64 stringLength = strlen(str);
    char *result = malloc(stringLength + 1);
    for (i = 0; i < stringLength; ++i)
    {
        result[i] = (char)tolower(str[i]);
    }
    result[i] = 0;

    return result;
}

ChatChannel * chat_new_channel(GameState *gameState, char *name)
{
    PrefixTree *c = ptree_find(gameState->channels, name);
    ChatChannel *channel = 0;

    if (!c)
    {
        char *lowerName;
        u64 nameLength = strlen(name);
        channel = malloc(sizeof(*channel));
        memset(channel, 0, sizeof(*channel));
        dynamic_array_init(channel->members, dbid);
        channel->name = malloc(nameLength + 1);
        memcpy(channel->name, name, nameLength + 1);

        lowerName = string_to_lowercase(name);
        ptree_add(gameState->channels, lowerName, channel);
        free(lowerName);
    }

    return channel;
}

ChatChannel * chat_get_channel(GameState *gameState, char *name)
{
    PrefixTree *node;
    ChatChannel *result = 0;
    char *lowerName = string_to_lowercase(name);

    node = ptree_find_nearest(gameState->channels, lowerName);
    if (node)
    {
        result = (ChatChannel *)node->data;
    }
    free(lowerName);

    return result;
}

void channel_add_member_by_id(ChatChannel *channel, dbid id)
{
    if (!channel_is_member(channel, id))
    {
        dynamic_array_push(channel->members, id);
    }
}

void channel_add_member(GameState *gameState, ChatChannel *channel, Entity *entity)
{
    char output_buffer[MAX_BUFFER_SIZE] = {0};

    if (!channel_is_member(channel, entity->id))
    {
        dynamic_array_push(channel->members, entity->id);

        snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s joined the channel.", entity->name);
        notify_channel(gameState, channel, output_buffer);
    }
}

void channel_remove_member(GameState *gameState, ChatChannel *channel, Entity *entity)
{
    char entity_output_buffer[MAX_BUFFER_SIZE] = {0};
    char channel_output_buffer[MAX_BUFFER_SIZE] = {0};
    u32 index;

    for (index = 0;
         index < dynamic_array_size(channel->members);
         ++index)
    {
        if (channel->members[index] == entity->id)
        {
            dynamic_array_erase(channel->members, index);
            break;
        }
    }

    snprintf(channel_output_buffer, MAX_BUFFER_SIZE - 1, "%s left the channel.", entity->name);
    notify_channel(gameState, channel, channel_output_buffer);

    snprintf(entity_output_buffer, MAX_BUFFER_SIZE - 1, "You have been removed from channel: %s", channel->name);
    notify_entity(entity, entity_output_buffer);
}

void channel_message(GameState *gameState, Entity *entity, char *channelName, char *message)
{
    PrefixTree *node;
    ChatChannel *channel = 0;
    char buffer[MAX_BUFFER_SIZE];
    char *lowerChannelName = 0;

    lowerChannelName = string_to_lowercase(channelName);
    node = ptree_find_nearest(gameState->channels, lowerChannelName);
    if (node)
    {
        channel = (ChatChannel *)node->data;
    }

    if (channel)
    {
        if (channel_is_member(channel, entity->id))
        {
            if (message[0] == TOKEN_POSE)
            {
                snprintf(buffer, MAX_BUFFER_SIZE - 1, "%s %s", entity->name, message + 1);
            } else {
                snprintf(buffer, MAX_BUFFER_SIZE - 1, "%s says, \"%s\"", entity->name, message);
            }
            notify_channel(gameState, channel, buffer);
        } else {
            snprintf(buffer, MAX_BUFFER_SIZE - 1, "You are not a member of %s", channel->name);
            notify_entity(entity, buffer);
        }
    } else {
        snprintf(buffer, MAX_BUFFER_SIZE - 1, "No such channel: %s", channelName);
        notify_entity(entity, buffer);
    }

    if (lowerChannelName)
        free(lowerChannelName);
}
