#ifndef STRING_H
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

typedef struct string_t
{
    i32 length;
    u8 *str;
} String;

String string_from_cstring(u8 *str)
{
    String result = {0};
    i32 length = (i32)strlen(str);
    result.length = length;
    result.str = xmalloc(length + 1);
    memcpy(result.str, str, length + 1);
    result.str[length] = 0;

    return result;
}

String string_copy(String str)
{
    String result;
    result.length = str.length;
    result.str = xmalloc(str.length + 1);
    memcpy(result.str, str.str, str.length);
    result.str[str.length] = 0;

    return result;
}

b32 is_end_of_line(char c)
{
    return (c == '\n' || c == '\r');
}

b32 is_whitespace(char c)
{
    return (c == ' ' || c == '\t' || c == '\f' || c == '\v' || is_end_of_line(c));
}

size_t eat_all_whitespace(char *content, size_t filePosition)
{
    while (content[filePosition] && is_whitespace(content[filePosition]))
    {
        ++filePosition;
    }

    return filePosition;
}

size_t one_past_first_whitespace(char *content, size_t position)
{
    while (content[position] && !is_whitespace(content[position++]))
        ;

    return position;
}

size_t one_past_first_character(char *content, u8 delimeter, size_t position)
{
    while (content[position] && content[position++] != delimeter)
        ;

    return position;
}

size_t one_past_end_of_line(char *content, size_t position)
{
    while (content[position] && !is_end_of_line(content[position++]))
        ;

    return position;
}


void split_string_by_whitespace(MemoryArena *arena, u8 *str, String *result)
{
    b32 is_splitting;
    String split;
    u64 prevPos;
    u64 strLength = strlen(str);
    u64 pos = eat_all_whitespace(str, 0);

    assert(result);

    is_splitting = true;
    while (is_splitting)
    {
        u64 splitSize;
        prevPos = pos;
        pos = one_past_first_whitespace(str, prevPos);
        // TODO(marshel): Eat extra whitespace too
        if (pos < strLength)
        {
            splitSize = pos - prevPos - 1;
        } else {
            splitSize = (i32)strlen(str + prevPos);
            is_splitting = false;
        }

        split.str = memory_arena_allocate(arena, (u32)(splitSize + 1));
        strncpy(split.str, str + prevPos, splitSize);
        split.str[splitSize] = 0;
        split.length = (i32)splitSize;

        dynamic_array_push(result, split);
    }
}

void split_string_by_character(MemoryArena *arena, u8 *str, u8 delimiter, String *result)
{
    b32 is_splitting;
    String split;
    u64 prevPos;
    u64 strLength = strlen(str);
    u64 pos = eat_all_whitespace(str, 0);

    assert(result);

    is_splitting = true;
    while (is_splitting)
    {
        u64 splitSize;
        prevPos = pos;
        pos = one_past_first_character(str, delimiter, prevPos);
        // TODO(marshel): Eat extra whitespace too
        if (pos < strLength)
        {
            splitSize = pos - prevPos - 1;
        } else {
            splitSize = (i32)strlen(str + prevPos);
            is_splitting = false;
        }

        split.str = memory_arena_allocate(arena, (u32)(splitSize + 1));
        strncpy(split.str, str + prevPos, splitSize);
        split.str[splitSize] = 0;
        split.length = (i32)splitSize;

        dynamic_array_push(result, split);
    }
}

i32 string_compare_insensitive(u8 *lhs, u8 *rhs, u64 count)
{
    i32 result = 0;
    u64 i;
    u8 a, b;

    if (lhs == rhs)
    {
        return 0;
    }

    if (lhs == 0)
    {
        return 1;
    }

    if (rhs == 0)
    {
        return -1;
    }

    for (i = 0; i < count; ++i)
    {
        a = lhs[i];
        b = rhs[i];

        if (a == 0 && b == 0) { break; }

        if (a == b || tolower(a) == tolower(b))
        {
            result = 0;
        } else {
            result = a - b;
            break;
        }
    }

    return result;
}

char * string_to_lowercase(MemoryArena *arena, char *str)
{
    u64 i;
    u64 stringLength = strlen(str);
    char *result = memory_arena_allocate(arena, (u32)(stringLength + 1));
    for (i = 0; i < stringLength; ++i)
    {
        result[i] = (char)tolower(str[i]);
    }
    result[i] = 0;

    return result;
}

i32 string_sort_partition(String *stringArray, i32 low, i32 high)
{
    String temp;
    String pivot = stringArray[high];
    i32 i, j;

    i = low - 1;
    for (j = low; j < high; ++j)
    {
        if (string_compare_insensitive(stringArray[j].str, pivot.str, stringArray[j].length) <= 0)
        {
            ++i;
            temp = stringArray[i];
            stringArray[i] = stringArray[j];
            stringArray[j] = temp;
        }
    }
    temp = stringArray[i + 1];
    stringArray[i + 1] = stringArray[high];
    stringArray[high] = temp;

    return i + 1;
}

void string_sort(String *stringArray, i32 low, i32 high)
{
    if (low < high)
    {
        i32 part = string_sort_partition(stringArray, low, high);
        string_sort(stringArray, low, part - 1);
        string_sort(stringArray, part + 1, high);
    }
}

#define STRING_H
#endif
