#ifndef FILENAME
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */
typedef void TimerFunction(GameState *gameState, void *args);

typedef struct timer_t
{
    struct timer_t *next, *prev;
    time_t interval;
    time_t nextRun;
    TimerFunction *timerFunction;
    void *timerFunctionArgs;
} Timer;

#define FILENAME
#endif

