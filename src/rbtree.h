#ifndef RBTREE_H
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

/*
 * red-black tree properties
 * 
 * 1. Every node is either red of black.
 * 2. The root is black.
 * 3. Every leaf (null) is black.
 * 4. If a node is red, then both its children are black.
 * 5. For each node, all simple paths from the node to descendant leaves
 *    contain the same number of black nodes.
 */

enum
{
    RBCOLOR_RED,
    RBCOLOR_BLACK,
};

typedef i32 dbid;
struct RBNode;

#define DBNodeHeader \
    struct RBNode *parent; \
    struct RBNode *left; \
    struct RBNode *right; \
    u8 color; \
    dbid id

typedef struct RBNode
{
    DBNodeHeader;
} RBNode;

typedef struct RBTree
{
    RBNode *root;
    RBNode *sentinel;
} RBTree;

void rb_init_tree(RBTree *tree)
{
    tree->sentinel = xcalloc(1, sizeof(RBNode));
    tree->sentinel->parent = 0;
    tree->sentinel->left = tree->sentinel;
    tree->sentinel->right = tree->sentinel;
    tree->sentinel->color = RBCOLOR_BLACK;
    tree->sentinel->id = 0;

    tree->root = tree->sentinel;
}

RBNode * rb_min(RBTree *tree, RBNode *node)
{
    RBNode *n = node;

    while (n->left != tree->sentinel)
    {
        n = n->left;
    }

    return n;
}

RBNode * rb_max(RBTree *tree, RBNode *node)
{
    RBNode *n = node;

    while (n->right != tree->sentinel)
    {
        n = n->right;
    }

    return n;
}

void rb_init(RBTree *tree, RBNode *node)
{
    node->parent = 0;
    node->left = tree->sentinel;
    node->right = tree->sentinel;
    node->color = RBCOLOR_RED;
    node->id = (dbid)(rb_max(tree, tree->root)->id + 1);
}

RBNode * rb_grandparent(RBNode *node)
{
    RBNode *result = 0;
    if (node->parent)
    {
        result = node->parent->parent;
    }

    return result;
}

RBNode * rb_uncle(RBNode *node)
{
    RBNode *result = 0;
    RBNode *g = rb_grandparent(node);
    if (g)
    {
        if (g->left == node->parent)
        {
            result = g->right;
        } else {
            result = g->left;
        }
    }

    return result;
}

RBNode * rb_search(RBTree *tree, dbid id)
{
    RBNode *node = tree->root;
    while (node != tree->sentinel)
    {
        if (node->id == id)
        {
            break;
        }

        if (node->id > id)
        {
            node = node->left;
        } else {
            node = node->right;
        }
    }

    if (node == tree->sentinel)
    {
        node = 0;
    }

    return node;
}

void rb_left_rotate(RBTree *tree, RBNode *node)
{
    RBNode *n = node->right;
    node->right = n->left;
    if (n->left != tree->sentinel)
    {
        n->left->parent = node;
    }
    n->parent = node->parent;
    if (node->parent == tree->sentinel)
    {
        tree->root = n;
    } else if (node == node->parent->left) {
        node->parent->left = n;
    } else {
        node->parent->right = n;
    }
    n->left = node;
    node->parent = n;
}

void rb_right_rotate(RBTree *tree, RBNode *node)
{
    RBNode *n = node->left;
    node->left = n->right;
    if (n->right != tree->sentinel)
    {
        n->right->parent = node;
    }
    n->parent = node->parent;
    if (node->parent == tree->sentinel)
    {
        tree->root = n;
    } else if (node == node->parent->left) {
        node->parent->left = n;
    } else {
        node->parent->right = n;
    }
    n->right = node;
    node->parent = n;
}

void rb_insert(RBTree *tree, RBNode *node)
{
    RBNode *y = tree->sentinel;
    RBNode *x = tree->root;
    while (x != tree->sentinel)
    {
        y = x;
        if (node->id < x->id)
        {
            x = x->left;
        } else {
            x = x->right;
        }
    }
    node->parent = y;
    if (y == tree->sentinel)
    {
        tree->root = node;
    } else if (node->id < y->id) {
        y->left = node;
    } else {
        y->right = node;
    }
    node->left = tree->sentinel;
    node->right = tree->sentinel;
    node->color = RBCOLOR_RED;

    // TODO(marshel): Fix tree
    while (node->parent && node->parent->color == RBCOLOR_RED)
    {
        RBNode *g = rb_grandparent(node);
        RBNode *u = rb_uncle(node);
        if (g->left == node->parent)
        {
            if (u && u->color == RBCOLOR_RED)
            {
                node->parent->color = RBCOLOR_BLACK;
                u->color = RBCOLOR_BLACK;
                g->color = RBCOLOR_RED;
                node = g;
            } else {
                if (node == node->parent->right)
                {
                    node = node->parent;
                    rb_left_rotate(tree, node);
                }
                node->parent->color = RBCOLOR_BLACK;
                g->color = RBCOLOR_RED;
                rb_right_rotate(tree, g);
            }
        } else {
            if (u && u->color == RBCOLOR_RED)
            {
                node->parent->color = RBCOLOR_BLACK;
                u->color = RBCOLOR_BLACK;
                g->color = RBCOLOR_RED;
                node = g;
            } else {
                if (node == node->parent->left)
                {
                    node = node->parent;
                    rb_right_rotate(tree, node);
                }
                node->parent->color = RBCOLOR_BLACK;
                g->color = RBCOLOR_RED;
                rb_left_rotate(tree, g);
            }
        }
    }
    tree->root->color = RBCOLOR_BLACK;
}

#define rbTransplant(tree, u, v)       \
{                                      \
    if (u->parent == tree->sentinel)   \
    {                                  \
        tree->root = v;                \
    } else if (u == u->parent->left) { \
        u->parent->left = v;           \
    } else {                           \
        u->parent->right = v;          \
    }                                  \
    v->parent = u->parent;             \
}

void rb_delete(RBTree *tree, RBNode *node)
{
    RBNode *y, *x;
    RBNode *sibling;
    u32 yOriginalColor;
    y = node;
    yOriginalColor = y->color;
    if (node->left == tree->sentinel)
    {
        sibling = node->right;
        rbTransplant(tree, node, node->right);
    } else if (node->right == tree->sentinel) {
        sibling = node->left;
        rbTransplant(tree, node, node->left);
    } else {
        y = rb_min(tree, node->right);
        yOriginalColor = y->color;
        sibling = y->right;
        if (y->parent == node)
        {
            sibling->parent = y;
        } else {
            rbTransplant(tree, y, y->right);
            y->right = node->right;
            y->right->parent = y;
        }
        rbTransplant(tree, node, y);
        y->left = node->left;
        y->left->parent = y;
        y->color = node->color;
    }

    x = sibling;
    if (yOriginalColor == RBCOLOR_BLACK)
    {
        while (x != tree->root && x->color == RBCOLOR_BLACK)
        {
            if (x == x->parent->left)
            {
                sibling = x->parent->right;
                if (sibling->color == RBCOLOR_RED)
                {
                    sibling->color = RBCOLOR_BLACK;
                    x->parent->color = RBCOLOR_RED;
                    rb_left_rotate(tree, x->parent);
                    sibling = x->parent->right;
                }

                if (sibling->left->color == RBCOLOR_BLACK && sibling->right->color == RBCOLOR_BLACK)
                {
                    sibling->color = RBCOLOR_RED;
                    x = x->parent;
                } else {
                    if (sibling->right->color == RBCOLOR_BLACK)
                    {
                        sibling->left->color = RBCOLOR_BLACK;
                        sibling->color = RBCOLOR_RED;
                        rb_right_rotate(tree, sibling);
                        sibling = x->parent->right;
                    }
                    sibling->color = x->parent->color;
                    x->parent->color = RBCOLOR_BLACK;
                    sibling->right->color = RBCOLOR_BLACK;
                    rb_left_rotate(tree, x->parent);
                    x = tree->root;
                }
            } else {
                sibling = x->parent->left;
                if (sibling->color == RBCOLOR_RED)
                {
                    sibling->color = RBCOLOR_BLACK;
                    x->parent->color = RBCOLOR_RED;
                    rb_right_rotate(tree, x->parent);
                    sibling = x->parent->left;
                }

                if (sibling->left->color == RBCOLOR_BLACK && sibling->right->color == RBCOLOR_BLACK)
                {
                    sibling->color = RBCOLOR_RED;
                    x = x->parent;
                } else {
                    if (sibling->left->color == RBCOLOR_BLACK)
                    {
                        sibling->right->color = RBCOLOR_BLACK;
                        sibling->color = RBCOLOR_RED;
                        rb_left_rotate(tree, sibling);
                        sibling = x->parent->left;
                    }
                    sibling->color = x->parent->color;
                    x->parent->color = RBCOLOR_BLACK;
                    sibling->left->color = RBCOLOR_BLACK;
                    rb_right_rotate(tree, x->parent);
                    x = tree->root;
                }
            }
        }
    }
}

void rb_clear_tree(RBTree *tree)
{
    Queue stack;
    RBNode *node; 

    DLIST_INIT(&stack);
    queue_push(&stack, &tree->root, sizeof(&tree->root));

    while (stack.next != &stack)
    {
        queue_pop(&stack, &node);

        if (node == tree->sentinel)
        {
            break;
        }

        if (node->left == tree->sentinel && node->right == tree->sentinel)
        {
            rb_delete(tree, node);
            continue;
        }

        if (node->left)
        {
            queue_push(&stack, &node->left, sizeof(&node->left));
        }

        if (node->right)
        {
            queue_push(&stack, &node->right, sizeof(&node->right));
        }
    }

    free(tree->sentinel);
    free(tree);
    tree = 0;
}

#define RBTREE_H
#endif
