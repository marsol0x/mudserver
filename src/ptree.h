#ifndef PTREE_H
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

// TODO(marshel): Add unicode support
// NOTE(marshel): This is just a binary tree; should it be?
// If I wanted this to be a prefix tree then I would need to track children in
// a single ordered, linked-list per node.

typedef struct _ptree_t
{
    struct _ptree_t *child, *next;
    u8 key;
    void *data;
} PrefixTree;

PrefixTree * ptree_init()
{
    PrefixTree *node = xcalloc(1, sizeof(PrefixTree));

    return node;
}

void ptree_add(PrefixTree *tree, u8 *name, void *data)
{
    PrefixTree *current = tree;
    PrefixTree *prev = tree;
    PrefixTree *n;
    u8 *ch = name;

    while (*ch)
    {
        if (current->key == *ch)
        {
            ++ch;
            if (!current->child)
            {
                current->child = ptree_init();
                current->child->key = *ch;
            }
            current = current->child;
        } else {
            prev = current;
            for (n = current->next; n; n = n->next)
            {
                if (n->key == *ch)
                {
                    current = n;
                    break;
                }
                prev = n;

                if (n->key > *ch)
                {
                    break;
                }
            }

            if (!n)
            {
                prev->next = ptree_init();
                prev->next->key = *ch;
                current = prev->next;
            } else {
                if (n->key > *ch)
                {
                    PrefixTree *p = prev->next;
                    prev->next = ptree_init();
                    prev->next->key = *ch;
                    current = prev->next;
                    current->next = p;
                }
            }
        }
    }

    current->data = data;
}

PrefixTree * ptree_find(PrefixTree *tree, u8 *name)
{
    PrefixTree *current = tree->next;
    u8 *ch = name;
    while (*ch && current)
    {
        if (current->key == *ch)
        {
            current = current->child;
            ++ch;
        } else {
            current = current->next;
        }
    }

    return current;
}

PrefixTree * ptree_find_nearest(PrefixTree *tree, u8 *name)
{
    u8 *ch = name;
    PrefixTree *current = tree;

    while (*ch && current)
    {
        if (current->key == *ch)
        {
            current = current->child;
            ++ch;
        } else {
            current = current->next;
        }
    }

    while (current && !current->data)
    {
        current = current->child;
    }

    return current;
}

#define PTREE_H
#endif
