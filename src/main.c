/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#define true (1==1)
#define false (!true)

typedef signed int b32;
typedef signed char i8; 
typedef signed short i16;
typedef signed int i32; 
typedef signed long long i64; 
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef float r32;
typedef double r64;

#ifdef _WIN32
#include <winsock2.h>
#else
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#endif

#ifndef _WIN32
#define closesocket close
#endif

#ifdef _WIN32
#define snprintf _snprintf
#endif


// TODO(marshel):
//  + Enable separation of global message types so we can selectvely message
//  sockets
//  + Make "Zero-Allocation Is Initialization" the default everywhere we can.
//  + Create Move functions independent from commands
//  + Improve linked lists
//      + LinkedList sorting
//  + Allow users to set their own alias?
//  + Create administrative commands
//      + Admin kick
//      + Create/delete room
//      + Create/delete door
//      + Teleport
//      + Get object info (examine)
//  + Auth/Auth
//      + Passwords
//      + Rights/permissions
//  + Colors (ascii)
//  + Bulletin Board
//  + Make an actual decision on passing Entity pointers or dbids
//  + Replace std string functions with my own, since I'm using unsigned char
//      + Not sure this is true anymore; make string handling consistent
//  + Actually close all of the sockets on shutdown
//  + Create consistent error messaging to Players
//  + Memory arenas
//  + Error messaging around insufficient parameters for commands 
//  + Should doors also be Entites? How do I save them?
//  + Channel chat history?
//  + Replace fgets with something that discards the newline
//  + Consider replacing DB with B+-Tree instead of the red-black tree
//  + Clean up SaveData objects after loading from save file
//
// DONE
//  + Move network input and output buffers/queues to the Connection struct,
//  rather than be on the Entity struct
//  + Convert entity contents to use dynamic arrays
//  + Inventory
//      + Needs to be an actual list of objects, rather than a linked list.
//      + Look at items in the inventory, in the room, and in viewable
//      continers
//      + Pick up items
//      + Drop items
//  + Object structures
//      + Single standard entity definition
//          + All entities can have various attributes applied to them, and I
//            don't see a reason why there should be any restriction. The
//            differences between entities are their function. This is fairly
//            PennMUSH-centric, but a room is a container the same way a user
//            might be in the sense of having an inventory.
//  + Object IDs stored in a red-black tree
//  + Improve prefix tree
//      + Need to be able to do a "closest" match, which requires sorting
//  + Short-names for commands
//      + Commands should be usable by incomplete names due to "closest match"
//      in the prefix tree.
//  + Improve linked lists
//      + Could be macro-based?
//  + Create administrative commands
//      + Player quit
//  + Private messages
//  + Improve private messages
//      + Last paged user should be pagable again without typing name
//  + Global chat
//  + Allow aliasing of "me" to be the current entity and "here" to be the
//    current entity's location in commands.
//  + Improve commands
//      + Create unified command parser for switches and args
//      + Command types?
//          + <command>
//          + <command> <args>
//          + <command> <target>=<args>
//          + <command>/<switch>
//          + <command>/<switch> <args>
//          + <command>/<switch> <target>=<args>
//  + Look functions independent from commands
//  + Dyanamic arrays
//  + Persistent object (room) storage
//      + Save the game
//          + Flat text file, built from depth-first iteration of in-memory
//          red-black tree database.

#define MAX_INPUT_SIZE 4096
#define MAX_BUFFER_SIZE (MAX_INPUT_SIZE * 2)

#ifndef _WIN32
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#else
#define MAX(a,b) max(a,b)
#endif

#define ARRAYLEN(a) (sizeof((a)) / sizeof((a)[0]))

#define TOKEN_LOOKUP '*'
#define TOKEN_NUMBER '#'
#define TOKEN_CHANNEL '+'
#define TOKEN_POSE ':'

i32 string_compare_insensitive(u8 *lhs, u8 *rhs, u64 count)
{
    i32 result = 0;
    u64 i;
    u8 a, b;

    for (i = 0; i < count; ++i)
    {
        a = lhs[i];
        b = rhs[i];

        if (a == 0 && b == 0) { break; }

        if (a == b || tolower(a) == tolower(b))
        {
            result = 0;
        } else {
            result = a - b;
            break;
        }
    }

    return result;
}

#include "queue.h"
#include "rbtree.h"
#include "ptree.h"
#include "game.h"
#include "entities.h"
#include "channels.h"

#include "dynamicarray.c"
#include "notify.c"
#include "sock.c"
#include "channels.c"
#include "look.c"
#include "commands.c"
#include "entities.c"
#include "game.c"

PrefixTree commands;
Command cmds[] = {
    { "@channel",   cmd_channel, },
    { "@describe",  cmd_describe, },
    { "@shutdown",  cmd_shutdown, },
    { "QUIT",       cmd_quit, },
    { "drop",       cmd_drop, },
    { "emit",       cmd_emit, },
    { "inventory",  cmd_inventory, },
    { "get",        cmd_get, },
    { "look",       cmd_look, },
    { "ooc",        cmd_ooc, },
    { "page",       cmd_page, },
    { "pose",       cmd_pose, },
    { "say",        cmd_say, },
    { "where",      cmd_where, },
    { "who",        cmd_who, },
};

i32 main(i32 argc, char **argv)
{
    GameState gameState;

    i32 port, server_sock;
    u32 i;

    if (argc < 2)
    {
        printf("Usage:\n\t%s <port>\n", argv[0]);
        exit(1);
    }

#ifdef WIN32
    if (init_winsock() != 0)
    {
        exit(1);
    }
#endif

    port = atoi(argv[1]);
    if ((server_sock = listen_socket(port)) == -1)
    {
        exit(1);
    }

    // NOTE(marshel): Add commands
    for (i = 0; i < ARRAYLEN(cmds); ++i)
    {
        ptree_add(&commands, cmds[i].name, cmds + i);
    }

    printf("Attempting to load save file.\n");
    if (!game_load(&gameState, "mudserver.save"))
    {
        printf("Save file failed to load. Initializing game.\n");
        game_initialize(&gameState, 0);
    }
    printf("Success.\n");

    DLIST_INIT(&gameState.output_queue);
    DLIST_INIT(&gameState.connections->input_queue);
    DLIST_INIT(&gameState.connections->output_queue);

    FD_ZERO(&gameState.masterfds);
    // Add the server socket to the listening to the master list
    FD_SET(server_sock, &gameState.masterfds);
    gameState.nfds = server_sock;

    gameState.running = true;
    while (gameState.running)
    {
        fd_set rfds = gameState.masterfds;
        fd_set wfds = gameState.masterfds;
        i32 new_nfds = gameState.nfds;
        if (select(gameState.nfds + 1, &rfds, &wfds, 0, 0) == -1)
        {
            perror("select");
        } else {
            // Reading
            i32 sock;
            u8 buffer[MAX_INPUT_SIZE];
            u8 output_buffer[MAX_BUFFER_SIZE];
            u32 size;
            Connection *conn;

            // NOTE(marshel): Close non-connected connections
            for (conn = gameState.connections->next; conn != gameState.connections; conn = conn->next)
            {
                if (!conn->isConnected)
                {
                    Connection *prev = conn->prev;
                    Entity *e = conn->entity;
                    new_nfds = entity_disconnect(&gameState, e);
                    DLIST_REMOVE(conn);
                    free(conn);
                    conn = prev;
                }
            }

            for (sock = 0; sock <= gameState.nfds; ++sock)
            {
                if (FD_ISSET(sock, &rfds))
                {
                    if (sock == server_sock)
                    {
                        char *message = "Who are you?";
                        conn = accept_connection(server_sock, &gameState.masterfds);
                        if (conn->sock == -1)
                        {
                            free(conn);
                            continue; // NOTE(marshel): Failed to establish client connection, so move on
                        }
                        DLIST_ADD(gameState.connections, conn);
                        conn->isConnected = true;
                        write_to_sock(conn->sock, message, strlen(message));
                        new_nfds = MAX(gameState.nfds, conn->sock);
                    } else {
                        for (conn = gameState.connections->next; conn != gameState.connections; conn = conn->next)
                        {
                            if (conn->sock == sock)
                            {
                                break;
                            }
                        }

                        if (conn != gameState.connections)
                        {
                            Entity *e = conn->entity;
                            u8 *recv_buffer = conn->input_buffer;
                            i32 bufferlen = MAX_BUFFER_SIZE - conn->input_buffer_index;
                            i32 received = 0;
                            if (bufferlen >= 0)
                            {
                                received = read_from_sock(sock, (recv_buffer + conn->input_buffer_index), (u32)bufferlen);

                                // NOTE(marshel): We either erred out or the user disconnected.
                                if (received == -1 || received == 0)
                                {
                                    new_nfds = entity_disconnect(&gameState, e);
                                    DLIST_REMOVE(conn);
                                    free(conn);
                                    continue;
                                }
                            }

                            process_input(conn, recv_buffer, received + conn->input_buffer_index);
                        }
                    }
                }
            }

            // NOTE: Writing global output to all socks
            while ((size = queue_pop(&gameState.output_queue, buffer)) != 0)
            {
                i32 sock;
                for (sock = 0; sock <= gameState.nfds; ++sock)
                {
                    // TODO(marshel): Do I really want to limit writes to only
                    // write-ready sockets?
                    if (FD_ISSET(sock, &wfds) && FD_ISSET(sock, &gameState.masterfds))
                    {
                        // TODO(marshel): There's a chance this loop could block the
                        // entire server. We might have to implement a buffer
                        // per socket and deal with send/recv once per loop
                        // iteration, rather than altogether as below.
                        write_to_sock(sock, buffer, (i64)size);
                    }
                }
            }

            // NOTE: Handle user input queues
            for (conn = gameState.connections->next; conn != gameState.connections; conn = conn->next)
            {
                Entity *e = conn->entity;
                while ((size = queue_pop(&conn->input_queue, buffer)) != 0)
                {
                    buffer[size] = 0;
                    memset(output_buffer, 0, MAX_BUFFER_SIZE);

                    // TODO(marshel): Is this the best place for this?
                    // TODO(marhsel): Replace this with authentication-based check
                    if (!conn->entity)
                    {
                        Entity *requestedEntity = entity_find_by_name(gameState.db, buffer);
                        if (requestedEntity == NULL)
                        {
                            Entity *newEntity = entity_create(&gameState, gameState.starting_room->id, EntityType_Player, buffer, 0);
                            conn->entity = newEntity;
                            newEntity->connection = conn;

                            snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "%s has connected.", newEntity->name);
                            queue_push(&gameState.output_queue, output_buffer, (u32)strlen(output_buffer));

                            dynamic_array_push(gameState.starting_room->contents, newEntity->id);
                            do_look_room(&gameState, newEntity, gameState.starting_room);
                        } else if (!requestedEntity->connection) {
                            Entity *location = (Entity *)rb_search(gameState.db, requestedEntity->location);
                            assert(location);

                            conn->entity = requestedEntity;
                            requestedEntity->connection = conn;
                            conn->isConnected = true;

                            do_look_room(&gameState, conn->entity, location);
                        } else {
                            char *message = "Username already taken. Please choose a different one.";
                            write_to_sock(conn->sock, message, strlen(message));
                        }
                    } else {
                        buffer[size] = 0;
                        do_command(&gameState, &commands, e, buffer);
                    }
                }
            }

            // NOTE(marshel): Writing user-specific output
            for (sock = 0; sock <= gameState.nfds; ++sock)
            {
                if (FD_ISSET(sock, &wfds) && FD_ISSET(sock, &gameState.masterfds))
                {
                    Connection *conn;
                    for (conn = gameState.connections->next; conn != gameState.connections; conn = conn->next)
                    {
                        if (conn->sock == (i32)sock)
                        {
                            break;
                        }
                    }

                    if (conn != gameState.connections)
                    {
                        while ((size = queue_pop(&conn->output_queue, buffer)) != 0)
                        {
                            write_to_sock(sock, buffer, size);
                        }
                    }
                }
            }

            gameState.nfds = new_nfds;
        }
    }

    // TODO(marshel): Do shutdown logic here
    game_save(&gameState, "mudserver.save");

    return 0;
}
