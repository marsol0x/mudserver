/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

#include <assert.h>
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define true (1==1)
#define false (!true)

typedef int32_t b32;
typedef int8_t i8; 
typedef int16_t i16;
typedef int32_t i32; 
typedef int64_t i64; 
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float r32;
typedef double r64;

#ifdef _WIN32
#include <winsock2.h>
#else
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#endif

#ifndef _WIN32
#define closesocket close
#endif

#ifdef _WIN32
#define snprintf _snprintf
#endif

// TODO(marshel):
//  + Create logging functions
//  + Auth/Auth
//      + Rights/permissions
//  + Create administrative commands
//      + Admin kick
//      + Create/delete room
//      + Create/delete door
//  + Enable separation of global message types so we can selectvely message
//  sockets (Wizard, Royal, etc)
//  + Make "Zero-Allocation Is Initialization" the default everywhere we can.
//  + Improve linked lists
//      + LinkedList sorting
//  + Bulletin Board
//  + Make an actual decision on passing Entity pointers or dbids
//  + Replace std string functions with my own, since I'm using unsigned char
//      + Not sure this is true anymore; make string handling consistent
//  + Create consistent error messaging to Players
//  + Error messaging around insufficient parameters for commands 
//      + Meta-programming opportunity here; we should be able to "configure"
//      commands so that the system knows whather or not a switch, target, or
//      general args are required and handle errors as part of parsing.
//  + Channel chat history?
//  + Consider replacing DB with B+-Tree instead of the red-black tree
//      + PennMUSH just uses a straight array of DB object pointers
//  + RPG elements
//      + Mobs
//      + Equipment
//      + Crafting?
//      + Stats
//  + String interning
//      + For things like full command names and "script" parameters, such as
//      colors
//  + Hash table for string interning and other static lookups
//
// DONE
//  + Move network input and output buffers/queues to the Connection struct,
//  rather than be on the Entity struct
//  + Convert entity contents to use dynamic arrays
//  + Inventory
//      + Needs to be an actual list of objects, rather than a linked list.
//      + Look at items in the inventory, in the room, and in viewable
//      continers
//      + Pick up items
//      + Drop items
//  + Object structures
//      + Single standard entity definition
//          + All entities can have various attributes applied to them, and I
//            don't see a reason why there should be any restriction. The
//            differences between entities are their function. This is fairly
//            PennMUSH-centric, but a room is a container the same way a user
//            might be in the sense of having an inventory.
//  + Object IDs stored in a red-black tree
//  + Improve prefix tree
//      + Need to be able to do a "closest" match, which requires sorting
//  + Short-names for commands
//      + Commands should be usable by incomplete names due to "closest match"
//      in the prefix tree.
//  + Improve linked lists
//      + Could be macro-based?
//  + Create administrative commands
//      + Get object info (examine)
//      + Player quit
//  + Private messages
//  + Improve private messages
//      + Last paged user should be pagable again without typing name
//  + Global chat
//  + Allow aliasing of "me" to be the current entity and "here" to be the
//    current entity's location in commands.
//  + Improve commands
//      + Create unified command parser for switches and args
//      + Command types?
//          + <command>
//          + <command> <args>
//          + <command> <target>=<args>
//          + <command>/<switch>
//          + <command>/<switch> <args>
//          + <command>/<switch> <target>=<args>
//  + Look functions independent from commands
//  + Dyanamic arrays
//  + Persistent object (room) storage
//      + Save the game
//          + Flat text file, built from depth-first iteration of in-memory
//          red-black tree database.
//  + Create administrative commands
//      + Teleport
//  + Create Move functions independent from commands
//  + Auth/Auth
//      + Passwords
//  + Actually close all of the sockets on shutdown
//  + Create login screen/prompt
//  + Timer functionality for periodic work, such as dumping the database to the disk
//  + Replace all malloc calls with calloc calls
//  + Parsing functions
//      + #dbid parser
//  + Create a string type
//  + Memory arenas
//      + Temporary memory arena cleared at the top of each loop
//  + Clean up SaveData objects after loading from save file
//  + Colors (ascii)
//  + VARARG printf-like notify functions?
//  + Allow users to set their own alias?
//  + Should doors also be Entites? How do I save them?
//  + notify_socket
//      + Useful for notifying sockets that are not yet full connections, such
//      as new connections.

#define MAX_INPUT_SIZE 4096
#define MAX_BUFFER_SIZE (MAX_INPUT_SIZE * 2)

#ifndef _WIN32
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#else
#define MAX(a,b) max(a,b)
#endif

#define ARRAYLEN(a) (sizeof((a)) / sizeof((a)[0]))

#define TOKEN_LOOKUP '*'
#define TOKEN_NUMBER '#'
#define TOKEN_CHANNEL '+'
#define TOKEN_POSE ':'

#include "memory.c"
#include "dynamicarray.c"
#include "string.h"
#include "color.h"

#include "queue.h"
#include "rbtree.h"
#include "ptree.h"
#include "game.h"
#include "entities.h"
#include "channels.h"
#include "timer.h"

#include "notify.c"
#include "sock.c"
#include "channels.c"
#include "entities.c"
#include "look.c"
#include "commands.c"
#include "game.c"
#include "timer.c"
#include "player.c"

PrefixTree commands;
Command cmds[] = {
    { "@alias",     cmd_alias, },
    { "@channel",   cmd_channel, },
    { "@create",    cmd_create, },
    { "@describe",  cmd_describe, },
    { "@destroy",   cmd_destroy, },
    { "@dump",      cmd_dump, },
    { "@name",      cmd_name, },
    { "@password",  cmd_password, },
    { "@shutdown",  cmd_shutdown, },
    { "@teleport",  cmd_teleport, },
    { "QUIT",       cmd_quit, },
    { "drop",       cmd_drop, },
    { "emit",       cmd_emit, },
    { "examine",    cmd_examine, },
    { "get",        cmd_get, },
    { "inventory",  cmd_inventory, },
    { "look",       cmd_look, },
    { "ooc",        cmd_ooc, },
    { "page",       cmd_page, },
    { "pose",       cmd_pose, },
    { "say",        cmd_say, },
    { "where",      cmd_where, },
    { "who",        cmd_who, },
};

i32 main(i32 argc, char **argv)
{
    GameState gameState = {0};
    Entity *starting_room = 0;
    char *saveFilePath = "mudserver.save";

    // TODO(marshel): Maybe the socket stuff should be in the GameState struct
    i32 port, server_sock;
    u32 i;

    if (argc < 2)
    {
        printf("Usage:\n\t%s <port>\n", argv[0]);
        exit(1);
    }

#ifdef WIN32
    if (init_winsock() != 0)
    {
        exit(1);
    }
#endif

    port = atoi(argv[1]);
    if ((server_sock = create_listen_socket(port)) == -1)
    {
        exit(1);
    }

    // NOTE(marshel): Add commands
    for (i = 0; i < ARRAYLEN(cmds); ++i)
    {
        ptree_add(&commands, cmds[i].name, cmds + i);
    }

    gamestate_init(&gameState);
    gameState.saveFilePath = saveFilePath;
    printf("Attempting to load save file.\n");
    if (!game_load(&gameState))
    {
        printf("Save file failed to load. Initializing game.\n");
        game_initialize(&gameState, 0);
    }
    printf("Success.\n");

    DLIST_INIT(&gameState.output_queue);
    DLIST_INIT(&gameState.connections.input_queue);
    DLIST_INIT(&gameState.connections.output_queue);

    FD_ZERO(&gameState.masterfds);
    // Add the server socket to the listening to the master list
    FD_SET(server_sock, &gameState.masterfds);
    gameState.nfds = server_sock;

    starting_room = entity_search(&gameState, gameState.starting_room);

    timer_startup(&gameState);

    gameState.running = true;
    while (gameState.running)
    {
        fd_set rfds = gameState.masterfds;
        fd_set wfds = gameState.masterfds;
        i32 new_nfds = gameState.nfds;

        // NOTE(marshel): Clear temporary memory
        memory_arena_reset(&gameState.temporary_memory);

        if (select(gameState.nfds + 1, &rfds, &wfds, 0, 0) == -1)
        {
            perror("select");
        } else {
            // Reading
            i32 sock;
            u8 buffer[MAX_INPUT_SIZE];
            u8 output_buffer[MAX_BUFFER_SIZE];
            u32 size;
            Connection *conn;

            // NOTE(marshel): Close non-connected connections
            for (conn = gameState.connections.next; conn != &gameState.connections; conn = conn->next)
            {
                if (!conn->isConnected)
                {
                    Connection *prev = conn->prev;
                    Entity *e = conn->entity;
                    entity_disconnect(&gameState, e);
                    new_nfds = close_connection(conn->sock, gameState.nfds, &gameState.masterfds);
                    DLIST_REMOVE(conn);
                    free(conn);
                    conn = prev;
                }
            }

            for (sock = 0; sock <= gameState.nfds; ++sock)
            {
                if (FD_ISSET(sock, &rfds))
                {
                    if (sock == server_sock)
                    {
                        conn = accept_connection(server_sock, &gameState.masterfds);
                        if (conn->sock == -1)
                        {
                            free(conn);
                            continue; // NOTE(marshel): Failed to establish client connection, so move on
                        }
                        DLIST_ADD(&gameState.connections, conn);
                        conn->isConnected = true;
                        notify_socket(conn->sock, "create <user> <password>\nconnect <user> <password>");
                        new_nfds = MAX(gameState.nfds, conn->sock);
                    } else {
                        for (conn = gameState.connections.next; conn != &gameState.connections; conn = conn->next)
                        {
                            if (conn->sock == sock)
                            {
                                break;
                            }
                        }

                        if (conn != &gameState.connections)
                        {
                            Entity *e = conn->entity;
                            u8 *recv_buffer = conn->input_buffer;
                            i32 bufferlen = MAX_BUFFER_SIZE - conn->input_buffer_index;
                            i32 received = 0;
                            if (bufferlen >= 0)
                            {
                                received = read_from_sock(sock, (recv_buffer + conn->input_buffer_index), (u32)bufferlen);

                                // NOTE(marshel): We either erred out or the user disconnected.
                                if (received == -1 || received == 0)
                                {
                                    entity_disconnect(&gameState, e);
                                    new_nfds = close_connection(conn->sock, gameState.nfds, &gameState.masterfds);
                                    DLIST_REMOVE(conn);
                                    free(conn);
                                    continue;
                                }
                            }

                            process_input(conn, recv_buffer, received + conn->input_buffer_index);
                        }
                    }
                }
            }

            // NOTE: Writing global output to all socks
            while ((size = queue_pop(&gameState.output_queue, buffer)) != 0)
            {
                i32 sock;
                for (sock = 0; sock <= gameState.nfds; ++sock)
                {
                    // TODO(marshel): Do I really want to limit writes to only
                    // write-ready sockets?
                    if (FD_ISSET(sock, &wfds) && FD_ISSET(sock, &gameState.masterfds))
                    {
                        // TODO(marshel): There's a chance this loop could block the
                        // entire server. We might have to implement a buffer
                        // per socket and deal with send/recv once per loop
                        // iteration, rather than altogether as below.
                        write_to_sock(sock, buffer, (i64)size);
                    }
                }
            }

            // NOTE: Handle user input queues
            for (conn = gameState.connections.next; conn != &gameState.connections; conn = conn->next)
            {
                Entity *e = conn->entity;
                while ((size = queue_pop(&conn->input_queue, buffer)) != 0)
                {
                    buffer[size] = 0;
                    memset(output_buffer, 0, MAX_BUFFER_SIZE);

                    // TODO(marshel): Is this the best place for this?
                    // TODO(marhsel): Replace this with authentication-based check
                    if (!conn->entity)
                    {
                        Entity *requestedEntity;

                        String *splitString = 0;
                        i8 *command = 0;
                        i8 *player = 0;
                        i8 *password = 0;

                        dynamic_array_init(splitString, String);
                        split_string_by_whitespace(&gameState.temporary_memory, buffer, splitString);

                        if (dynamic_array_size(splitString) == 1 && string_compare_insensitive("QUIT", splitString[0].str, 4) == 0)
                        {
                            new_nfds = close_connection(conn->sock, gameState.nfds, &gameState.masterfds);
                        } else if (dynamic_array_size(splitString) >= 3) {
                            command  = splitString[0].str;
                            player   = splitString[1].str;
                            password = splitString[2].str;

                            if (string_compare_insensitive("create", command, 5) == 0)
                            {
                                // NOTE(marshel): Create Player
                                requestedEntity = entity_find_by_name(gameState.db, player);
                                if (!requestedEntity)
                                {
                                    Entity *newEntity = player_create(&gameState, conn, starting_room, player, password);
                                    notify_all_connected(&gameState, "%s has connected.", newEntity->name.str);
                                } else {
                                    notify_socket(conn->sock, "That name is already taken.");
                                }
                            } else if (string_compare_insensitive("connect", command, 7) == 0) {
                                // NOTE(marshel): Connect Player
                                requestedEntity = entity_find_by_name(gameState.db, player);
                                if (requestedEntity)
                                {
                                    if (strcmp(requestedEntity->password, password) == 0)
                                    {
                                        player_connect(&gameState, conn, requestedEntity);
                                        notify_all_connected(&gameState, "%s has connected.", requestedEntity->name.str);
                                    } else {
                                        notify_socket(conn->sock, "Invalid name or password.");
                                    }
                                } else {
                                    notify_socket(conn->sock, "Invalid name or password.");
                                }
                            } else {
                                notify_socket(conn->sock, "Invalid command.");
                            }
                        } else {
                            notify_socket(conn->sock, "Invalid number of parameters.");
                        }

                        // TODO(marshel): Can I reuse this?
                        dynamic_array_free(splitString);
                    } else {
                        buffer[size] = 0;
                        do_command(&gameState, &commands, e, buffer);
                    }
                }
            }

            // NOTE(marshel): Writing user-specific output
            {
                Connection *conn;
                for (conn = gameState.connections.next;
                     conn != &gameState.connections;
                     conn = conn->next)
                {
                    i32 sock = conn->sock;
                    if (FD_ISSET(sock, &wfds) && FD_ISSET(sock, &gameState.masterfds))
                    {
                        while ((size = queue_pop(&conn->output_queue, buffer)) != 0)
                        {
                            write_to_sock(sock, buffer, size);
                        }
                    }
                }
            }
            gameState.nfds = new_nfds;
        }

        timer_run(&gameState);
    }

    // TODO(marshel): Do shutdown logic here
    game_save(&gameState);

    {
        i32 sock;
        for (sock = 0; sock <= gameState.nfds; ++sock)
        {
            closesocket(sock);
        }

    }
    closesocket(server_sock);

    return 0;
}
