#ifndef ENTITIES_H
/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

#include "rbtree.h"

#define MAX_NAME_SIZE 30
#define MAX_PASSWORD_SIZE 512
#define MAX_DESCRIPTION_SIZE 1024
#define MAX_CHANNEL_MEMBERSHIP 256

#define NULL_ENTITY -1

enum
{
    EntityType_None,

    EntityType_Player,
    EntityType_Room,
    EntityType_Thing,

    EntityType_End,
};

typedef struct door_t
{
    dbid dest;
    String name;
    String *aliases; // NOTE(marshel): Dynamic array
} Door;

typedef struct entity_t
{
    DBNodeHeader;

    Connection *connection;

    dbid lastPaged;
    dbid location; // NOTE(marshel): Rooms have a location of -1 to differentiate them

    // NOTE(marshel): Dynamic arrays
    dbid *contents;
    Door *doors;

    i32 type;
    String name;
    u8 alias[MAX_NAME_SIZE];
    u8 password[MAX_PASSWORD_SIZE]; // NOTE(marshel): This is just plaintext, don't use this in real life!
    u8 description[MAX_DESCRIPTION_SIZE];
} Entity;

u8 * entity_type_to_cstring(MemoryArena *arena, i32 type)
{
    u8 *result = memory_arena_allocate(arena, 10);

    switch (type)
    {
        case EntityType_Player:
        {
            strcpy(result, "Player");
        } break;

        case EntityType_Room:
        {
            strcpy(result, "Room");
        } break;

        case EntityType_Thing:
        {
            strcpy(result, "Thing");
        } break;

        default:
        {
            strcpy(result, "None");
        } break;
    }

    return result;
}

// TODO(marshel): Make these names consistent
Entity * entity_get_from_string(GameState *gameState, Entity *entity, u8 *str);
Entity * entity_find_by_name(RBTree *tree, u8 *name);
void entity_add_content(GameState *gameState, Entity *target, Entity *object);
void entity_drop_content(GameState *gameState, Entity *entity, Entity *object);

#define entity_search(gameState, id) (Entity *)rb_search((gameState)->db, (id))

#define ENTITIES_H
#endif
