/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

#include <errno.h>

#ifdef _WIN32
#include <Ws2tcpip.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#include "sock.h"

#ifdef _WIN32
i32 init_winsock()
{
    WSADATA wsaData;
    return WSAStartup(0x0202, &wsaData);
}
#endif

i32 create_listen_socket(i32 port)
{
    struct sockaddr_in addr;
    socklen_t addrlen;
    i32 sock;
    char optval;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("listen_socket - sock");
        return -1;
    }

    addrlen = sizeof(addr);
    memset(&addr, 0, addrlen);
    addr.sin_family = AF_INET;
    addr.sin_port = htons((u16) port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    optval = true;
    // NOTE(marshel): We're ignoring any errors from setsockopt
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));


    if (bind(sock, (struct sockaddr *) &addr, addrlen) == -1)
    {
        perror("listen_socket - bind");
        return -1;
    }

    if (listen(sock, MAXQUEUE) == -1)
    {
        perror("listen_socket - listen");
        return -1;
    }

    return sock;
    
}

Connection * accept_connection(i32 server_sock, fd_set *masterfds)
{
    Connection *conn;
    i32 sock;
    struct sockaddr addr;
    socklen_t addrlen;

    addrlen = sizeof(addr);
    if ((sock = accept(server_sock, &addr, &addrlen)) == -1)
    {
        perror("accept_connection - accept");
        return NULL;
    }
    conn = xcalloc(1, sizeof(Connection));
    conn->sock = sock;
    DLIST_INIT(&conn->input_queue);
    DLIST_INIT(&conn->output_queue);

    FD_SET(conn->sock, masterfds);
    printf("New connection: %d\n", conn->sock);

    return conn;
}

i32 close_connection(i32 sock, i32 nfds, fd_set *masterfds)
{
    i32 new_nfds;

    if (nfds == sock)
    {
        new_nfds = nfds - 1;
    }

    printf("Connection disconnect: %d\n", sock);
    closesocket(sock);
    FD_CLR(sock, masterfds);

    if (nfds == sock)
    {
        for (new_nfds = nfds; new_nfds > 2; --new_nfds)
        {
            if (FD_ISSET(new_nfds, masterfds))
            {
                break;
            }
        }
    } else {
        new_nfds = nfds;
    }

    return new_nfds;
}

i32 read_from_sock(i32 sock, u8 *buffer, u64 bufferlen)
{
    i32 received_count = (i32) recv(sock, buffer, bufferlen, 0);
    return received_count;
}

void write_to_sock(i32 sock, u8 *buffer, i64 bufferlen)
{
    i64 length = bufferlen;
    i32 bufferpos = 0;
    do
    {
        i64 sent_count = 0;
        sent_count = send(sock, (buffer + bufferpos), (size_t)length, 0);
        if (sent_count == -1)
        {
            perror("send");
            break;
        }
        length -= sent_count;
        bufferpos += sent_count;
    } while (length > 0);
    send(sock, "\n", 1, 0);
}

void process_input(Connection *conn, u8 *raw_input, i32 len)
{
    // TODO(marshel): Should I truncate and accept input data?
    u8 *index;
    u8 *marker;
    u8 *end = raw_input + len;
    for (index = marker = raw_input; index < end; ++index)
    {
        if (*index == '\r' || *index == '\n')
        {
            *index = 0;
            if (index - marker <= MAX_INPUT_SIZE)
            {
                // store command
                if (index - marker > 0)
                {
                    u8 buf[MAX_INPUT_SIZE];
                    strncpy(buf, marker, MAX_INPUT_SIZE);
                    queue_push(&conn->input_queue, buf, (u32) (index - marker));
                }
            } else {
                // input overflow
                notify_entity(conn->entity, (u8 *) "<Input Overflow>");
            }
            marker = index + 1;
        }
    }

    if (marker < index)
    {
        i32 len_to_end = (i32) (end - marker);
        if (len_to_end > 0 && len_to_end < (MAX_BUFFER_SIZE - conn->input_buffer_index - 1))
        {
            i32 i = 0;
            i32 j = (i32) (marker - raw_input);
            while (i < len_to_end)
            {
                raw_input[i++] = raw_input[j++];
            }
            conn->input_buffer_index = i;
        }
    } else {
        conn->input_buffer_index = 0;
    }
}
