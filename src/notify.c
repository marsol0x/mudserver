/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */

void notify_entity(Entity *e, char *message)
{
    if (e->connection)
    {
        queue_push(&e->connection->output_queue, message, (u32)strlen(message));
    }
}

void notify_room(GameState *gameState, Entity *room, u8 *message)
{
    u32 index;

    for (index = 0; index < dynamic_array_size(room->contents); ++index)
    {
        Entity *entity = entity_search(gameState, room->contents[index]);
        if (entity && entity->type == EntityType_Player && entity->connection)
        {
            notify_entity(entity, message);
        }
    }
}

void notify_channel(GameState *gameState, ChatChannel *channel, char *message)
{
    char output_buffer[MAX_BUFFER_SIZE] = {0};
    u32 index;

    snprintf(output_buffer, MAX_BUFFER_SIZE - 1, "<%s> %s", channel->name, message);
    for (index = 0;
         index < dynamic_array_size(channel->members);
         ++index)
    {
        Entity *entity = entity_search(gameState, channel->members[index]);
        if (entity)
        {
            notify_entity(entity, output_buffer);
        }
    }
}
