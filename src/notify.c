/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Author: Marshel Helsper $
   $Notice: $
   ======================================================================== */
#include <stdarg.h>
#include "sock.h"

void notify_entity_with_args(Entity *e, char *format, va_list args)
{
    u8 message_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len;
    assert(e);

    len = vsnprintf(message_buffer, MAX_BUFFER_SIZE - 1, format, args);
    queue_push(&e->connection->output_queue, message_buffer, len);
}

void notify_entity(Entity *e, char *format, ...)
{
    if (e && e->connection)
    {
        va_list args;
        va_start(args, format);
        notify_entity_with_args(e, format, args);
        va_end(args);
    }
}

void notify_room(GameState *gameState, Entity *room, u8 *message, ...)
{
    u32 index;
    va_list args;
    va_start(args, message);
    for (index = 0; index < dynamic_array_size(room->contents); ++index)
    {
        Entity *entity = entity_search(gameState, room->contents[index]);
        if (entity && entity->type == EntityType_Player && entity->connection)
        {
            notify_entity_with_args(entity, message, args);
        }
    }
    va_end(args);
}

void notify_channel(GameState *gameState, ChatChannel *channel, char *message, ...)
{
    char output_buffer[MAX_BUFFER_SIZE] = {0};
    u32 index;
    va_list args;

    va_start(args, message);
    vsnprintf(output_buffer, MAX_BUFFER_SIZE - 1, message, args);
    for (index = 0;
         index < dynamic_array_size(channel->members);
         ++index)
    {
        Entity *entity = entity_search(gameState, channel->members[index]);
        if (entity)
        {
            notify_entity(entity, "<%s> %s", channel->name.str, output_buffer);
        }
    }
    va_end(args);
}

void notify_all_connected(GameState *gameState, char *message, ...)
{
    Connection *c;
    va_list args;

    va_start(args, message);
    for (c = gameState->connections.next; c != &gameState->connections; c = c->next)
    {
        if (c->entity)
        {
            notify_entity_with_args(c->entity, message, args);
        }
    }
    va_end(args);
}

void notify_socket(int sock, char *message, ...)
{
    u8 message_buffer[MAX_BUFFER_SIZE] = {0};
    i32 len;
    va_list args;
    va_start(args, message);

    len = vsnprintf(message_buffer, MAX_BUFFER_SIZE - 1, message, args);
    write_to_sock(sock, message_buffer, len);

    va_end(args);
}
