@echo off

if not exist ..\build mkdir ..\build
pushd ..\build
ctime -begin mudserver.ctm

cl /nologo /Od /MTd /Zi /FC /Fd"mudserver.pdb" /Fm"mudserver.map" /Fe"mudserver.exe" ..\src\main.c /link Ws2_32.lib

ctime -end mudserver.ctm
popd
