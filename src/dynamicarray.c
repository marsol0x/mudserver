/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */
// TODO(marshel): Custom allocators?

#define dynamic_array_capacity(a)     (a ? (*(dynamic_array__raw(a))) : 0)
#define dynamic_array_size(a)         (a ? (*(dynamic_array__raw(a) + 1)) : 0)
#define dynamic_array_init(a, n)      dynamic_array_grow((void **)&a, sizeof(n))
#define dynamic_array_push(a, n)      dynamic_array__push((void **)&a, (void *)&n, sizeof(n))
#define dynamic_array_insert(a, i, n) dynamic_array__insert((void **)&a, i, (void *)&n, sizeof(n))
#define dynamic_array_erase(a, i)     dynamic_array__erase((void **)&a, i, sizeof(a[0]));
#define dynamic_array_clear(a)        dynamic_array__clear((void **)&a, sizeof(a[0]));
#define dynamic_array_free(a)         dynamic_array__free((void **)&a)

#define dynamic_array__raw(a)         ((u32 *)a - 2)

#define DYNAMIC_ARRAY_INITIAL_CAPACITY 32

// TODO(marshel): Simplify this
void dynamic_array_grow(void **array, size_t element_size)
{
    if (*array == 0)
    {
        u32 *size, *capacity;
        void *newArray = xmalloc((sizeof(u32) * 2) + (element_size * DYNAMIC_ARRAY_INITIAL_CAPACITY));
        capacity = (u32 *)newArray;
        size = (u32 *)newArray + 1;
        *array = (u32 *)newArray + 2;
        *capacity = DYNAMIC_ARRAY_INITIAL_CAPACITY;
        *size = 0;
    } else {
        if (dynamic_array_capacity(*array) <= dynamic_array_size(*array))
        {
            void *newArray;
            u32 newCapacity = dynamic_array_capacity(*array) * 2;
            newArray = xrealloc(dynamic_array__raw(*array), (sizeof(u32) * 2) + (newCapacity * element_size));
            if (newArray)
            {
                u32 *capacity = (u32 *)newArray;
                *array = (void *)((u32 *)newArray + 2);
                *capacity = newCapacity;
            }
        }
    }
}

void dynamic_array__push(void **array, void *element, size_t element_size)
{
    dynamic_array_grow(array, element_size);
    memcpy((u8 *)*array + (dynamic_array_size(*array) * element_size), element, element_size);
    dynamic_array__raw(*array)[1]++;
}

void dynamic_array__insert(void **array, u32 index, void *element, size_t element_size)
{
    if (*array)
    {
        u8 *a = (u8 *)*array + (index * element_size);
        u8 *b = (u8 *)*array + ((index + 1) * element_size);

        dynamic_array_grow(array, element_size);
        memmove(a, b, dynamic_array_size(*array) - index);
        memcpy((u8 *)*array + (index * element_size), element, element_size);

        dynamic_array__raw(*array)[1]++;
    }
}

void dynamic_array__erase(void **array, u32 index, size_t element_size)
{
    if (*array)
    {
        u8 *a = (u8 *)*array + (index * element_size);
        u8 *b = a + element_size;
        memmove(a, b, dynamic_array_capacity(*array) - index);

        dynamic_array__raw(*array)[1]--;
    }
}

void dynamic_array__clear(void **array, size_t element_size)
{
    if (*array)
    {
        // TODO(marshel): Do we need to do a real clear or can we just reset
        // the size of the array?
        memset((u8 *)*array, 0, dynamic_array_size(*array) * element_size);
        dynamic_array__raw(*array)[1] = 0;
    }
}

void dynamic_array__free(void **array)
{
    void *a = dynamic_array__raw(*array);
    free(a);
    *array = 0;
}
