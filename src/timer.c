/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */
#include <time.h>

void timer_register(GameState *gameState, time_t interval, TimerFunction *timerFunction, void *args)
{
    time_t currentTime = time(0);
    Timer *newTimer = xcalloc(1, sizeof(Timer));

    newTimer->interval = interval;
    newTimer->nextRun = currentTime + interval;
    newTimer->timerFunction = timerFunction;
    newTimer->timerFunctionArgs = args;

    DLIST_ADD(gameState->timers, newTimer);
}

void timer_startup(GameState *gameState)
{
    gameState->timers = xcalloc(1, sizeof(Timer));
    DLIST_INIT(gameState->timers);

    // TODO(marshel): The interval should be configurable
    timer_register(gameState, 300, game_save_timer, 0);
}

void timer_run(GameState *gameState)
{
    Timer *timer;
    time_t currentTime = time(0);

    for (timer = gameState->timers->next;
         timer != gameState->timers;
         timer = timer->next)
    {
        if (timer->nextRun <= currentTime)
        {
            timer->timerFunction(gameState, timer->timerFunctionArgs);
            timer->nextRun = currentTime + timer->interval;
        }
    }
}
