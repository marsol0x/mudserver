/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

// NOTE(marshel): Some dyanmic array testing.
{
    i32 *ints = NULL;
    i32 i;
    i32 x;

    for(i = 0; i < 10; i++) {
        dynamic_array_push(ints, i);
    }

    dynamic_array_erase(ints, 4);
    x = 99;
    dynamic_array_insert(ints, 4, x);

    printf("ints has the following contents:\n\n");

    {
        u32 i;
        for(i = 0; i < dynamic_array_size(ints); i++) {
            printf("%i", ints[i]);
            if(i != dynamic_array_size(ints) - 1) {
                printf(", ");
            }
        }
        dynamic_array_free(ints);
        printf("\n\n");
    }

    exit(1);
}
