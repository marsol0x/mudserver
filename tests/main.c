/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdint.h>
#include <stdio.h>

#include "unity.h"

int32_t main(int32_t argc, char **argv)
{

    printf("Hello, Tests!\n");

    return 0;
}
