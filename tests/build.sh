#!/bin/bash

errors="-Weverything"
#disabled_errors="-Wno-missing-prototypes -Wno-padded -Wno-shadow"
#temp_disabled="-Wno-missing-variable-declarations -Wno-unreachable-code -Wno-unused-macros -Wno-unused-parameter -Wno-cast-align -Wno-pointer-sign -Wno-sign-conversion"
#temp_disabled_osx="-Wno-unreachable-code-return"

if [ "`uname`" = "Darwin" ]
then
    temp_disabled="$temp_disabled $temp_disabled_osx"
else
    temp_disabled="$temp_disabled $temp_disabled_nix"
fi

mkdir -p ../bin
/usr/bin/clang -std=c99 -O0 -g $errors $disabled_errors $temp_disabled -o ../bin/test_mudserver unity.c main.c
